package usercollection

import (
	"database/sql"
	"fmt"
	"giftsphere/db"
	"giftsphere/model"
	"strconv"
)

// Errors
var (
	ErrNotFoundUser        = fmt.Errorf("UserCollection: not found User by ID")
	ErrNotFoundUserByEmail = fmt.Errorf("UserCollection: not found User by email")
	ErrUsernameBad         = fmt.Errorf("UserCollection: bad username")
	ErrUsernameExist       = fmt.Errorf("UserCollection: username exist")
)

type FiltersByUser struct {
	SortField     string
	SortDirection string
	Limit         int64
	Offset        int64
	UserID        int64
	Email         string
	Name          string
}

func Run() {
}

func GetUserByName(name string, connDB db.ConnDB) (user *model.User, err error) {
	list, err := GetUserList(FiltersByUser{
		Limit: 1,
		Name:  name,
	}, connDB)
	if err != nil {
		return
	}

	if len(list) == 0 {
		return nil, ErrUsernameBad
	}

	return list[0], nil
}

func GetUserByEmail(email string, connDB db.ConnDB) (user *model.User, err error) {
	list, err := GetUserList(FiltersByUser{
		Limit: 1,
		Email: email,
	}, connDB)
	if err != nil {
		return
	}

	if len(list) == 0 {
		return nil, ErrNotFoundUserByEmail
	}

	return list[0], nil
}

func GetUserByID(ID int64, connDB db.ConnDB) (user *model.User, err error) {
	list, err := GetUserList(FiltersByUser{
		Limit:  1,
		UserID: ID,
	}, connDB)
	if err != nil {
		return
	}

	if len(list) == 0 {
		return nil, ErrNotFoundUser
	}

	return list[0], nil
}

func GetUserList(filters FiltersByUser, connDB db.ConnDB) (list model.UserList, err error) {
	params := []interface{}{}
	list = model.UserList{}
	var email sql.NullString
	var facebookId sql.NullString

	where := ``
	orderBy := ``

	if filters.UserID > 0 {
		if where != "" {
			where += " AND "
		}

		where += "id = " + strconv.Itoa(int(filters.UserID))
	}

	if filters.Name != "" {
		if where != "" {
			where += " AND "
		}

		params = append(params, filters.Name)
		where += "name = $" + strconv.Itoa(len(params))
	}

	if filters.Email != "" {
		if where != "" {
			where += " AND "
		}

		params = append(params, filters.Email)
		where += "email = $" + strconv.Itoa(len(params))
	}

	if filters.SortField != `` && filters.SortDirection == `` {
		orderBy += ` ORDER BY ` + filters.SortField
	} else if filters.SortField != `` && filters.SortDirection != `` {
		orderBy += ` ORDER BY ` + filters.SortField + ` ` + filters.SortDirection
	}

	userPagination := ``

	if filters.Limit == 0 {
		userPagination += " LIMIT 10" + " OFFSET " + strconv.Itoa(int(filters.Offset))
	} else if filters.Limit > 0 {
		userPagination += " LIMIT " + strconv.Itoa(int(filters.Limit)) + " OFFSET " + strconv.Itoa(int(filters.Offset))
	}

	if where != "" {
		where = "WHERE " + where
	}

	rows, err := connDB.Query(`SELECT id,  email, money,  cristal, last_connected, last_disconnected, total_time, password, name FROM users
`+where+`
`+orderBy+`
`+userPagination, params...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		user := &model.User{}
		err = rows.Scan(&user.ID, &email, &user.Money, &user.Cristal, &user.LastConnected, &user.LastDisconnected, &user.TotalTime, &user.Password, &user.Name)
		if err != nil {
			return
		}
		user.Email = email.String
		user.FacebookID = facebookId.String
		list = append(list, user)
	}

	return
}

func GetTotalUserCount(connDB db.ConnDB) (total int, err error) {
	rows, err := connDB.Query(`SELECT COUNT(id) AS total_count FROM users`)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&total)
		if err != nil {
			return
		}
	}

	return
}

func GetTotalUserList(connDB db.ConnDB) (ids []int, err error) {
	rows, err := connDB.Query(`SELECT "id" FROM users`)
	if err != nil {
		return
	}

	defer rows.Close()
	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err != nil {
			return
		}
		ids = append(ids, id)
	}

	return
}

func CreateUser(user *model.User, connDB db.ConnDB) (err error) {
	err = connDB.QueryRow(`
INSERT INTO users
(email, password, name)
VALUES ($1, $2, $3) RETURNING id;`,
		user.Email, user.Password, user.Name,
	).Scan(&user.ID)
	if err != nil {
		return
	}

	return
}
