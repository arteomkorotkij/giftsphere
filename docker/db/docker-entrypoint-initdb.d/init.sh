#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE USER homesport WITH PASSWORD 'homesport';
  CREATE DATABASE homesport;
  GRANT ALL PRIVILEGES ON DATABASE homesport TO homesport;
EOSQL
