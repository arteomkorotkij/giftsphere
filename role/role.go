package role

const (
	Unauthorized Role = iota // Unauthorized = 0
	User         Role = iota
	Admin        Role = iota
)

var str = []string{
	"Unauthorized",
	"User",
	"Admin",
}

func (r Role) String() string {
	return str[r]
}

type Role int
