package session

import (
	"context"

	"golang.org/x/crypto/acme/autocert"

	"giftsphere/db"
	"github.com/garyburd/redigo/redis"
)

const redisTLSExpirationTime = 2 * 30 * 24 * 60 * 60 // in seconds -> 2 mount

// TLSCache implements Cache using a directory on the local filesystem.
// If the directory does not exist, it will be created with 0700 permissions.
type TLSCache struct{}

// Get reads a certificate data from the specified file name.
func (d TLSCache) Get(ctx context.Context, name string) (b []byte, err error) {
	conn, err := db.GetRedisDatabase(db.REDIS_COMMON_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	b, err = redis.Bytes(conn.Do("GET", "tls:"+name))
	if err == redis.ErrNil {
		err = autocert.ErrCacheMiss
	}

	return
}

func (d TLSCache) Put(ctx context.Context, name string, data []byte) (err error) {
	conn, err := db.GetRedisDatabase(db.REDIS_COMMON_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	_, err = conn.Do("SET", "tls:"+name, data, "EX", redisTLSExpirationTime)
	return
}

// Delete removes the specified file name.
func (d TLSCache) Delete(ctx context.Context, name string) (err error) {
	conn, err := db.GetRedisDatabase(db.REDIS_COMMON_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	_, err = conn.Do("DEL", "tls:"+name)
	return
}
