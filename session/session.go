package session

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/gocraft/web"

	"giftsphere/db"
	"giftsphere/tools"
)

const (
	RedisUserExpirationTime = 1 * 30 * 24 * 60 * 60 // in seconds
)

func Update(s *Session, ctx context.Context, w http.ResponseWriter) (err error) {
	if s.SessID == "" {
		return fmt.Errorf("Update() s.ID is empty")
	}

	conn, err := db.GetRedisDatabase(db.REDIS_SESSION_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	_, err = conn.Do("SET", s.SessID, s.ToJSON(), "EX", RedisUserExpirationTime)
	if err != nil {
		return
	}

	expiration := time.Now().Add(RedisUserExpirationTime * time.Second)
	cookie := http.Cookie{Name: "s", Value: s.SessID, Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)
	return
}

func Add(w web.ResponseWriter, s *Session, ctx context.Context) (err error) {
	if s.SessID == "" {
		s.SessID = tools.NewID()
	}

	conn, err := db.GetRedisDatabase(db.REDIS_SESSION_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	_, err = conn.Do("SET", s.SessID, s.ToJSON(), "EX", RedisUserExpirationTime)
	if err != nil {
		return
	}

	expiration := time.Now().Add(RedisUserExpirationTime * time.Second)
	cookie := http.Cookie{Name: "s", Value: s.SessID, Expires: expiration, Path: "/"}
	http.SetCookie(w, &cookie)

	return
}

func Get(r *web.Request, ctx context.Context) (s *Session, err error) {
	sessID := ""
	cookie, err := r.Cookie("s")
	if err == nil && cookie.Value != "" {
		sessID = cookie.Value
	} else {
		sessID = r.Header.Get("Authorization")
	}
	if sessID == "" {
		return nil, nil
	}

	conn, err := db.GetRedisDatabase(db.REDIS_SESSION_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	b, err := redis.Bytes(conn.Do("GET", sessID))
	if err != nil {
		return
	}

	return NewSessionFromJson(b, ctx)
}

func Del(ctx context.Context, sessionID string) (err error) {
	conn, err := db.GetRedisDatabase(db.REDIS_SESSION_DB, ctx)
	if err != nil {
		return
	}
	defer conn.Close()

	_, err = conn.Do("Del", sessionID)
	return
}
