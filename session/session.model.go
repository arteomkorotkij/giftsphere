//go:generate easyjson $GOFILE
package session

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/mailru/easyjson"

	"giftsphere/model"
	"giftsphere/role"
	"giftsphere/tools"
)

var (
	EmptyUser = &model.User{}
)

//easyjson:json
type Session struct {
	User              *model.User `json:"-"`
	SessID            string      `json:"sessId"`
	UserID            int64       `json:"userId"`
	IsAdmin           bool        `json:"isAdmin"`
	Role              role.Role   `json:"role"`
	NotificationToken string      `json:"notificationToken"`
	dontUpdate        bool
	context           context.Context
}

func NewSessionFromJson(b []byte, ctx context.Context) (s *Session, err error) {
	s = &Session{
		User:    EmptyUser,
		context: ctx,
	}
	err = easyjson.Unmarshal(b, s)
	return
}

func NewSession(ctx context.Context) *Session {
	return &Session{
		User:    EmptyUser,
		context: ctx,
	}
}

func (s *Session) GenSessionID(userID int64) {
	s.SessID = tools.IntStr(userID) + ":" + tools.NewID()
}

func (s *Session) IsFastSessionIDOrEmptyOrAdmin() bool {
	return s.SessID == "" || !strings.Contains(s.SessID, ":") || strings.Contains(s.SessID, "-1")
}

func (s *Session) Update(w http.ResponseWriter) error {
	if s.dontUpdate {
		return nil
	}

	return Update(s, s.context, w)
}

func (s *Session) Delete(w http.ResponseWriter) error {
	cookie := http.Cookie{Name: "s", Value: "", MaxAge: -1}
	http.SetCookie(w, &cookie)
	cookie = http.Cookie{Name: "s", Value: "", MaxAge: -1, Path: "/"}
	http.SetCookie(w, &cookie)
	w.WriteHeader(http.StatusOK)

	return Del(s.context, s.SessID)
}

func (s *Session) DontUpdate() {
	s.dontUpdate = true
}

func (s *Session) ToJSON() string {
	b, err := easyjson.Marshal(s)
	if err != nil {
		fmt.Println("Error:Session.ToJSON(): ", err)
	}

	return string(b)
}

func (s *Session) SetUser(user *model.User) {
	s.User = user
}
