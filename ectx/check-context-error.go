package ectx

import (
	"context"
	"net/http"
	"strconv"

	"giftsphere/errorR"
)

func CheckContextError(ctx *Context) (err error) {
	if err2, ok := ctx.Context.Value(FieldContextError).(*errorR.Error); ok {
		return err2
	}

	if err = ctx.Context.Err(); err != nil && !ctx.IsContextError {
		ctx.IsContextError = true
		var status int
		msg := err.Error()
		msgLog := "context error: " + err.Error() + ". process time log id: "

		switch err {
		case context.DeadlineExceeded:
			status = http.StatusGatewayTimeout
		case context.Canceled:
			status = http.StatusInternalServerError
			msg = "server canceled request"
		}

		if err = ctx.ProcessTimeLog.WriteToDB(); err != nil {
			return
		}

		msgLog += strconv.Itoa(ctx.ProcessTimeLog.ID)
		err = ctx.NewError(status, msg, msgLog)
		ctx.Context = context.WithValue(ctx.Context, FieldContextError, err) //nolint
	}

	return
}
