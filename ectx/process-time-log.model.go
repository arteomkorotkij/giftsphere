package ectx

import (
	"sync"
	"time"
)

type ProcessTimeLog struct {
	mutex       sync.RWMutex
	durationLog []time.Duration
	nameLog     []string
	ID          int
}

func (p *ProcessTimeLog) Append(name string, duration time.Duration) {
	defer p.mutex.Unlock()
	p.mutex.Lock()
	p.durationLog = append(p.durationLog, duration)
	p.nameLog = append(p.nameLog, name)
}

func (p *ProcessTimeLog) ForEach(fn func(name string, duration time.Duration)) {
	defer p.mutex.RUnlock()
	p.mutex.RLock()
	for i := 0; i < len(p.nameLog); i++ {
		fn(p.nameLog[i], p.durationLog[i])
	}
}

func (p *ProcessTimeLog) WriteToDB() error {
	return nil
}

func (p *ProcessTimeLog) SetTimeWork(nameWork string, t time.Time) {
	p.Append(nameWork, time.Since(t))
}

func NewProcessTimeLog() *ProcessTimeLog {
	return &ProcessTimeLog{
		durationLog: []time.Duration{},
		nameLog:     []string{},
	}
}
