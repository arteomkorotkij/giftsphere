package ectx

import (
	"encoding/json"
	"fmt"

	"github.com/gocraft/web"
	"github.com/mailru/easyjson"
)

func (c *Context) ParseJson(r *web.Request, obj interface{}) (err error) {
	if ej, ok := obj.(easyjson.Unmarshaler); ok {
		return easyjson.UnmarshalFromReader(r.Body, ej)
	}

	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(obj)
	_ = r.Body.Close()
	if err != nil {
		return fmt.Errorf("bad json data: " + err.Error())
	}

	return
}
