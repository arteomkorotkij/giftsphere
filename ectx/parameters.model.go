package ectx

import (
	"fmt"
	"github.com/shopspring/decimal"
	"strconv"
	"strings"
)

type Parameters map[string]string

func (p Parameters) Set(name, value string) {
	p[name] = value
}

func (p Parameters) GetString(name string) (string, error) {
	v, ok := p[name]
	if !ok {
		return v, fmt.Errorf("parametr '" + name + "' is empty")
	}

	return v, nil
}

func (p Parameters) GetStringArr(name string) ([]string, error) {
	pV := p[name]
	if pV == "" {
		return []string{}, fmt.Errorf("parametr '" + name + "' is empty")
	}

	return strings.Split(pV, ","), nil
}

func (p Parameters) GetInt(name string) (int, error) {
	pV := p[name]
	if pV == "" {
		return 0, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := strconv.Atoi(pV)
	if err != nil {
		return 0, fmt.Errorf("parametr '" + name + "' is not integer")
	}

	return v, nil
}

func (p Parameters) GetInt16(name string) (int16, error) {
	pV := p[name]
	if pV == "" {
		return 0, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := strconv.ParseInt(pV, 10, 16)
	if err != nil {
		return 0, fmt.Errorf("parametr '" + name + "' is not integer")
	}

	return int16(v), nil
}

func (p Parameters) GetInt32(name string) (int32, error) {
	pV := p[name]
	if pV == "" {
		return 0, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := strconv.ParseInt(pV, 10, 32)
	if err != nil {
		return 0, fmt.Errorf("parametr '" + name + "' is not integer")
	}

	return int32(v), nil
}

func (p Parameters) GetInt64(name string) (int64, error) {
	pV := p[name]
	if pV == "" {
		return 0, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := strconv.ParseInt(pV, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("parametr '" + name + "' is not big integer number")
	}

	return int64(v), nil
}

func (p Parameters) GetFloat32(name string) (float32, error) {
	pV := p[name]
	if pV == "" {
		return 0, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := strconv.ParseFloat(pV, 32)
	if err != nil {
		return 0, fmt.Errorf("parametr '" + name + "' is not float number")
	}

	return float32(v), nil
}

func (p Parameters) GetFloat64(name string) (float64, error) {
	pV := p[name]
	if pV == "" {
		return 0, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := strconv.ParseFloat(pV, 64)
	if err != nil {
		return 0, fmt.Errorf("parametr '" + name + "' is not big float number")
	}

	return v, nil
}

func (p Parameters) GetDecimal(name string) (decimal.Decimal, error) {
	pV := p[name]
	if pV == "" {
		return decimal.Decimal{}, fmt.Errorf("parametr '" + name + "' is empty")
	}

	v, err := decimal.NewFromString(pV)
	if err != nil {
		return decimal.Decimal{}, fmt.Errorf("parametr '" + name + "' is not decimal")
	}

	return v, nil
}

func (p Parameters) GetBool(name string) (bool, error) {
	pV := p[name]
	if pV == "" {
		return false, fmt.Errorf("parametr '" + name + "' is empty")
	}

	switch pV {
	case "true", "TRUE", "True":
		return true, nil
	case "false", "FALSE", "False":
		return false, nil
	default:
		return false, fmt.Errorf("parametr '" + name + "' is not boolean")
	}
}
