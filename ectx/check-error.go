package ectx

import (
	"context"
)

func CheckError(ctx *Context, inputErr error, status int, forWeb string) (err error) {
	if inputErr == context.DeadlineExceeded || inputErr == context.Canceled {
		return CheckContextError(ctx)
	}

	if inputErr != nil {
		err = ctx.NewError(status, forWeb, inputErr.Error())
	}

	return
}
