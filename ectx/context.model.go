package ectx

import (
	"context"
	"time"

	"github.com/gocraft/web"

	"giftsphere/db"
	"giftsphere/errorR"
	"giftsphere/session"
)

var FieldContextError = "error"

type Context struct {
	Stop           context.CancelFunc
	Context        context.Context
	IsContextError bool
	Session        *session.Session
	ProcessTimeLog *ProcessTimeLog
	Parameters     *Parameters
	DB             db.ConnDB
	tx             *db.Tx
	Lang           string
	PathLang       string
	DesignID       int64
	IsDevMode      bool
}

func (ctx *Context) HaveTx() bool {
	return ctx.tx != nil
}

func (ctx *Context) GetTx() (tx *db.Tx, err error) {
	if ctx.tx != nil {
		tx = ctx.tx
	} else {
		tx, err = db.NewTx(ctx.Context, ctx.Stop)
		if err == nil {
			ctx.tx = tx
		}
	}

	return
}

func (ctx *Context) SetTx(tx *db.Tx) {
	ctx.tx = tx
}

func (ctx *Context) CheckError(w web.ResponseWriter, r *web.Request, err error) bool {
	if err != nil {
		ctx.WriteJson(w, r, err, nil)
		return true
	}
	return false
}

func (ctx *Context) NewError(status int, forWeb, forLog string) *errorR.Error {
	return errorR.NewError(status, forWeb, forLog, ctx.IsDevMode)
}

func NewDefaultTestContext() (*Context, *db.Tx, error) { // For tests
	ctx := context.Background()
	ctx, stop := context.WithTimeout(ctx, time.Second*10)
	tx, err := db.NewTx(ctx, stop)
	if err != nil {
		return nil, tx, err
	}

	return &Context{
		Stop:           stop,
		Context:        ctx,
		DB:             tx,
		ProcessTimeLog: NewProcessTimeLog(),
	}, tx, nil
}
