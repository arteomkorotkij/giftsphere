package ectx

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gocraft/web"
	"github.com/mailru/easyjson"

	"giftsphere/errorR"
)

// Errors
var (
	ErrSkip = fmt.Errorf("Skip error")
)

func (c *Context) WriteJson(w web.ResponseWriter, r *web.Request, err error, d interface{}) {
	if err == ErrSkip {
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	if err != nil {
		if errorReq, ok := err.(*errorR.Error); ok && !c.IsDevMode {
			w.WriteHeader(errorReq.Status)

			_, _, err = easyjson.MarshalToHTTPResponseWriter(errorReq, w)
			if err != nil {
				fmt.Println(err)
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			_, _, err = easyjson.MarshalToHTTPResponseWriter(errorR.Error{Status: 400, Msg: err.Error()}, w)
			if err != nil {
				fmt.Println(err)
			}
		}

		return
	}

	if ej, ok := d.(easyjson.Marshaler); ok {
		_, _, err = easyjson.MarshalToHTTPResponseWriter(ej, w)
		if err != nil {
			fmt.Println(err)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(d)
	if err != nil {
		fmt.Println(err)
	}
}
