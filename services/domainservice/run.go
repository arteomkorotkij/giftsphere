package domainservice

import (
	"fmt"
	"strings"

	"giftsphere/conf"
)

// Errors
var (
	ErrDomainNameIsEmpty = fmt.Errorf("DomainService: Domain names is empty")
)

type DomainControl struct {
	Domain string
	IP     string
	Lang   string
}

var (
	domainControl = map[string]*DomainControl{}
)

func Run() {
	domainNames := conf.Config.Str("domains")
	if domainNames == "" {
		panic(ErrDomainNameIsEmpty)
	}

	domainList := strings.Split(domainNames, ",")
	for _, domainIpLangStr := range domainList {
		domainIpLang := strings.Split(domainIpLangStr, "|")
		lang := domainIpLang[2]

		domainControl[lang] = &DomainControl{
			Domain: domainIpLang[0],
			IP:     domainIpLang[1],
			Lang:   lang,
		}
	}
}

func GetDomainControls() map[string]*DomainControl {
	return domainControl
}
