package domainservice

func GetDomainByLang(lang string) *DomainControl {
	dc := domainControl[lang]
	if dc != nil {
		return dc
	}

	return domainControl["*"]
}

func GetFullDomainByLang(subDomain, lang string) string {
	if subDomain == "" {
		return ""
	}

	dc := domainControl[lang]
	if dc == nil {
		dc = domainControl["*"]
	}

	return subDomain + "." + dc.Domain
}
