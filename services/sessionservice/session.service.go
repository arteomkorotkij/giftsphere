package sessionservice

import (
	"context"

	"github.com/gocraft/web"

	"giftsphere/session"
)

func CreateSession(rw web.ResponseWriter, ctx context.Context) (s *session.Session, err error) {
	s = session.NewSession(ctx)
	err = session.Add(rw, s, ctx)
	return
}

func TemporarySession(rw web.ResponseWriter, ctx context.Context) (s *session.Session, err error) {
	s = session.NewSession(ctx)
	return
}

func DeleteSession(rw web.ResponseWriter, ctx context.Context, s *session.Session) (err error) {
	return s.Delete(rw)
}

func GetSession(req *web.Request, ctx context.Context) (s *session.Session, err error) {
	return session.Get(req, ctx)
}
