package staticservice

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"html/template"
	"mime"
	"net/http"
	"path"
	"strings"

	"github.com/andybalholm/brotli"
	packr "github.com/gobuffalo/packr/v2"

	"giftsphere/conf"
	"giftsphere/model"
)

type I18N map[string]interface{}

var (
	box       *packr.Box
	templates = template.New("parent").Funcs(funcMap)
	static    = map[string]*model.StaticFile{}
	i18n      = map[string]I18N{}
	funcMap   = template.FuncMap{
		"prefix": func(s, prefix string) bool {
			return strings.HasPrefix(s, prefix)
		},
		"translate": func(lang, key, interpolateKey string, interpolateValue interface{}) string {
			var interpolate map[string]string
			if interpolateKey != "" {
				interpolate = map[string]string{interpolateKey: fmt.Sprint(interpolateValue)}
			}

			res, err := GetTranslate(model.Lang(lang), key, interpolate)
			if err != nil {
				panic(err)
			}

			return res
		},
		"int32Range": func(start, end int32) []int32 {
			n := end - start + 1
			result := make([]int32, n)
			for i := int32(0); i < n; i++ {
				result[i] = start + i
			}
			return result
		},
		"sumInt32": func(a, b int32) int32 {
			return a + b
		},
	}
)

func GetStatic(name string) *model.StaticFile {
	return static[name]
}

func GetTmp() *template.Template {
	return templates
}

func GetTranslate(lang model.Lang, key string, interpolate map[string]string) (translate string, err error) {
	langStr := string(lang)
	currentI18N := GetI18N(langStr)
	if currentI18N == nil {
		return "", fmt.Errorf("staticservice.GetTranslateByStrLang(): not found lang: " + langStr)
	}

	var ok bool
	translate, ok = currentI18N[key].(string)
	if !ok || translate == "" {
		return "", fmt.Errorf("staticservice.GetTranslateByStrLang(): not found key: " + key)
	}

	for interKey, value := range interpolate {
		translate = strings.Replace(translate, "{{"+interKey+"}}", value, -1)
	}

	return
}

func GetI18N(lang string) I18N {
	lang += ".json"
	return i18n[lang]
}

func init() {
	box = packr.New("Customer", "../../customer-ui2/dist")
	for _, path := range box.List() {
		if strings.HasPrefix(path, "template") {
			putTmp(path)
		} else if strings.HasPrefix(path, "assets") {
			putStatic(path)
		} else if strings.HasPrefix(path, "i18n") {
			putI18n(path)
		}
	}

	box = &packr.Box{}
}

func putStatic(name string) {
	file, err := box.Find(name)
	if err != nil {
		panic(err)
	}

	name = name[7:] // remove prefix "assets/"
	name = strings.Replace(name, `\`, "/", -1)
	sFile := &model.StaticFile{
		Name: name,
		Data: file,
	}

	if !conf.IsDev() && !strings.HasSuffix(name, ".png") && !strings.HasSuffix(name, ".jpg") {
		buf := bytes.NewBuffer([]byte{})
		compressor, err := gzip.NewWriterLevel(buf, gzip.BestCompression)
		if err != nil {
			panic(err)
		}

		_, err = compressor.Write(sFile.Data)
		if err != nil {
			_ = compressor.Close()
			panic(err)
		}

		_ = compressor.Close()
		sFile.CompressGzip = buf.Bytes()

		bufBr := bytes.NewBuffer([]byte{})
		compressorBr := brotli.NewWriterLevel(bufBr, brotli.BestCompression)

		_, err = compressorBr.Write(sFile.Data)
		if err != nil {
			_ = compressorBr.Close()
			panic(err)
		}

		_ = compressorBr.Close()
		sFile.CompressBr = bufBr.Bytes()
	}

	var contentType string
	var mimetype string
	fileExt := path.Ext(name)
	if fileExt != "" {
		mimetype = mime.TypeByExtension(fileExt)
	}
	if mimetype == "" {
		mimetype = http.DetectContentType(file)
	}
	if mimetype != "" {
		contentType = mimetype
	}

	sFile.ContentType = contentType
	static[name] = sFile
}

func putTmp(name string) {
	data, err := box.FindString(name)
	if err != nil {
		panic(err)
	}

	name = name[9:] // remove prefix "template/"
	name = strings.Replace(name, `\`, "/", -1)

	templates = templates.New(name)
	if templates, err = templates.Parse(data); err != nil {
		panic(err)
	}
}

func putI18n(name string) {
	file, err := box.Find(name)
	if err != nil {
		panic(err)
	}

	name = name[5:] // remove prefix "i18n/"
	name = strings.Replace(name, `\`, "/", -1)
	data := I18N{}

	if err := json.Unmarshal(file, &data); err != nil {
		panic(err)
	}

	i18n[name] = data
}
