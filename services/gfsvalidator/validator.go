package gfsvalidator

import (
	"reflect"

	"github.com/go-playground/validator/v10"
	"github.com/shopspring/decimal"

	"giftsphere/model"
)

var validate *validator.Validate

func init() {
	validate = validator.New()

	validate.RegisterCustomTypeFunc(decimalFloatValue, decimal.Decimal{})

	if err := validate.RegisterValidation("isSubscriptionEngine", isSubscriptionEngine); err != nil {
		panic(err)
	}
	if err := validate.RegisterValidation("isLang", isLangValidator); err != nil {
		panic(err)
	}
}

func ValidateStruct(s interface{}) error {
	return validate.Struct(s)
}

func decimalFloatValue(v reflect.Value) interface{} {
	n, ok := v.Interface().(decimal.Decimal)

	if !ok {
		return nil
	}

	f, _ := n.Float64()
	return f
}

func isSubscriptionEngine(fl validator.FieldLevel) bool {
	engine := fl.Field().Interface().(model.SubscriptionEngine)
	return engine.IsEngine()
}

func isLangValidator(fl validator.FieldLevel) bool {
	lang := fl.Field().Interface().(model.Lang)

	for _, l := range model.LangList {
		if l == lang {
			return true
		}
	}

	return false
}
