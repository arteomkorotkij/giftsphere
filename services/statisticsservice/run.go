package statisticsservice

import (
	"giftsphere/db"
	"giftsphere/tools"
)

const MEMORY_ANALYTICS_KEY = "analytics_key"

var (
	analyticsKey string
)

func Run() {
	analyticsKey, _ = tools.GetSetting(db.PlainDB, MEMORY_ANALYTICS_KEY)
}

func GetAnalyticsKey() string {
	return analyticsKey
}

func SetAnalyticsKey(key string) (err error) {
	err = tools.SetSetting(db.PlainDB, MEMORY_ANALYTICS_KEY, key)
	if err != nil {
		return
	}

	analyticsKey = key
	return
}
