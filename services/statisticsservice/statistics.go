package statisticsservice

import "time"

var (
	countLastDayConnections int64
	currentCountConnection  int64
	durationLastConnection  time.Duration
)

type Statistics struct {
	CountLastDayConnections   int64         `json:"countLastDayConnections"`
	CurrentCountConnection    int64         `json:"currentCountConnection"`
	DurationLastConnection    time.Duration `json:"durationLastConnection"`
	DurationLastConnectionStr string        `json:"durationLastConnectionStr"`
}

func GetStatistics() *Statistics {
	return &Statistics{
		CountLastDayConnections:   countLastDayConnections,
		CurrentCountConnection:    currentCountConnection,
		DurationLastConnection:    durationLastConnection,
		DurationLastConnectionStr: durationLastConnection.String(),
	}
}

func IncCurrentConnection() {
	currentCountConnection++
	countLastDayConnections++
}

func DecCurrentConnection(duration time.Duration) {
	currentCountConnection--
	if duration > 0 {
		durationLastConnection = duration
	}
}
