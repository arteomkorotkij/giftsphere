const rimraf = require("rimraf");
const fs = require("fs");
const interpolate = require('./src/interpolate/interpolate');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const crypto = require('crypto');


init().catch(err => console.error('error', err));
async function init() {
    rimraf.sync("dist");

    const { stdout, stderr } = await exec('parcel build src/main.scss');
    if (stderr) {
        throw `${stderr}: ${stdout}`;
    }
    console.log(stdout);

    await require('esbuild').build({
        entryPoints: ['src/main.ts'],
        bundle: true,
        target: 'chrome58,firefox57,safari11,edge16'.split(','),
        sourcemap: true,
        minify: true,
        treeShaking: true,
        outfile: 'dist/main.js',
    });

    let mainJsData = fs.readFileSync('dist/main.js', {encoding: 'utf-8'});
    let mainJsMapData = fs.readFileSync('dist/main.js.map', {encoding: 'utf-8'});
    const mainJsHash = crypto.createHash('md5').update(mainJsData).digest('hex');

    console.log('dist/main.js.map', (mainJsMapData.length / 1024).toFixed(2), 'KB');
    console.log('dist/main.js    ', (mainJsData.length / 1024).toFixed(2), 'KB');
    console.log();

    let mainCssData = fs.readFileSync('dist/main.css', {encoding: 'utf-8'});
    const mainCssHash = crypto.createHash('md5').update(mainCssData).digest('hex');

    mainJsData = mainJsData.replace(`//# sourceMappingURL=main.js.map`, `//# sourceMappingURL=main.${mainJsHash}.js.map`);
    fs.writeFileSync('dist/main.js', mainJsData);
    mainCssData = mainCssData.replace(`/*# sourceMappingURL=/main.css.map */`, `/*# sourceMappingURL=main.${mainCssHash}.css.map */`);
    fs.writeFileSync('dist/main.css', mainCssData);

    fs.renameSync(`dist/main.js`, `dist/main.${mainJsHash}.js`);
    fs.renameSync(`dist/main.js.map`, `dist/main.${mainJsHash}.js.map`);
    fs.renameSync(`dist/main.css`, `dist/main.${mainCssHash}.css`);
    fs.renameSync(`dist/main.css.map`, `dist/main.${mainCssHash}.css.map`);

    await interpolate.init();
}
