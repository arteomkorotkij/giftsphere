// import "babel-polyfill";

import {Core, HttpClient, Router, Validator} from "./code/core";
import {App} from "./code/app";
import {Signup} from "./code/signup";
import {Login} from "./code/login";
import {DataSource} from "./code/data-source";
import {Logout} from "./code/logout";

const entities = {
  HttpClient,
  Router,
  Validator,
  Core,
  App,
  Signup,
  Login,
  DataSource,
  Logout,
};
