declare var Common: CommonI;
declare var paypal: any;

export interface CommonI {
  lang: string;
  analytics: string;
  chatJWT: string;
  paypal: boolean;
  stripe: string;
  googleReCaptchaKey: string;
  isAuth: boolean;
  userId: number;
  userEmail: string;
  hasLangVersion: {[lang: string]: string};
}
