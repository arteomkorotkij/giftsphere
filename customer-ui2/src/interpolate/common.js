const common = {
  appName: 'Farm',
  host: process.env.HOST || 'https://farm.com',
};
common.defaultOgSite = `${common.host}/static/img/favicon/android-chrome-512x512.png?v=2`;

module.exports = common;
