module.exports = {
  index: {
    path: '/',
    template: 'pages/index.html',
    title: 'HOME_S_TITLE',
  },
  login: {
    path: '/login',
    template: 'pages/login.html',
  },
  signup: {
    path: '/signup',
    template: 'pages/signup.html',
  },
  spheres: {
    path: '/spheres',
    template: 'pages/spheres.html',
  },
  sphereDetails: {
    path: '/spheres/',
    pathPrefix: true,
    template: 'pages/sphere-details.html',
  },
};
