module.exports = {
  ...require('./common'),
  routers: require('./routers'),
  apStatus: {
    ok: 'active',
    pending: 'pending',
    inactive: 'inactive',
  },
  stripeStatus: {
    active: 'active',
    pending: 'pending',
    inactive: 'inactive',
  }
};
