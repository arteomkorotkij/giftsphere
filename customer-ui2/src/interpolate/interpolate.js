const go = require('go');
const TemplatesPlugin = require('go-plugin-templates').TemplatesPlugin;
const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require("path");
const minify = require('html-minifier').minify;
const crypto = require('crypto');

go.use(TemplatesPlugin);
const data = require('./data');
const templateDir = 'src/template/';
const distFolder = 'dist';
const distDir = path.resolve(__dirname, `../../${distFolder}`);

async function init(isWindows) {
  data.routersArr = [];
  for (let routersKey in data.routers) {
    data.routers[routersKey].key = routersKey;
    data.routersArr.push(data.routers[routersKey]);
  }

  const cssDir = `${distDir}/assets/css`;
  const jsDir = `${distDir}/assets/js`;
  const browserI18NDir = `${distDir}/assets/browser_i18n`;
  const browserI18nHashMap = {};
  mkdirp.sync(cssDir);
  mkdirp.sync(jsDir);
  mkdirp.sync(browserI18NDir);

  await new Promise((res, rej) => {
    fs.readdir(`./${distFolder}/i18n`, async (err, files) => {
      if (err) {
        rej(err);
        return console.error(err);
      }

      for (let i = 0; i < files.length; i++) {
        const fileName = files[i];
        const i18n = await readJSON(`${distDir}/i18n/${fileName}`);
        for (let i18nKey in i18n) {
          if (!i18nKey.startsWith('BROWSER_')) {
            delete i18n[i18nKey];
          }
        }

        const fileData = JSON.stringify(i18n);
        const lang = fileName.slice(0, fileName.lastIndexOf('.'));
        // const md5sum = crypto.createHash('md5');
        // md5sum.update(fileData);
        browserI18nHashMap[lang] = crypto.createHash('md5').update(fileData).digest('hex');
        fs.writeFileSync(`${distDir}/assets/browser_i18n/${fileName}`, fileData);
      }

      res();
    });
  });
  data.browserI18nHashMap = JSON.stringify(browserI18nHashMap);

  data.cssArr = [];
  data.jsArr = [];
  fs.readdir(`./${distFolder}/`, async (err, files) => {
    if (err) {
      return console.error(err);
    }

    for (let i = 0; i < files.length; i++) {
      const fileName = files[i];
      //
      if (fileName.slice(fileName.length - 4) === '.css') {

        if (isWindows) {
          let rows = await readFile(`${distDir}/${fileName}`);
          rows = rows.replace(fileName, `static/css/${fileName}`);
          fs.writeFileSync(`${cssDir}/${fileName}`, rows);
          fs.unlinkSync(`${distDir}/${fileName}`);
        } else {
          data.cssArr.push(`/static/css/${fileName}`);
          fs.renameSync(`${distDir}/${fileName}`, `${cssDir}/${fileName}`);
        }

        //
      } else if (fileName.slice(fileName.length - 3) === '.js') {

        if (isWindows) {
          let rows = await readFile(`${distDir}/${fileName}`);
          rows = rows.replace(fileName, `static/js/${fileName}`);
          fs.writeFileSync(`${jsDir}/${fileName}`, rows);
          fs.unlinkSync(`${distDir}/${fileName}`);
        } else {
          data.jsArr.push(`/static/js/${fileName}`);
          fs.renameSync(`${distDir}/${fileName}`, `${jsDir}/${fileName}`);
        }

        //
      } else if (fileName.slice(fileName.length - 7) === '.js.map') {
        fs.rename(`${distDir}/${fileName}`, `${jsDir}/${fileName}`, err => {
          if (err) throw err
        });
      } else if (fileName.slice(fileName.length - 8) === '.css.map') {
        fs.rename(`${distDir}/${fileName}`, `${cssDir}/${fileName}`, err => {
          if (err) throw err
        });
      }
      //
    }
  });

  const templatesPaths = [];
  await go.processTemplates(
    data,
    `${templateDir}**`,
    file => {
      const p = `${distFolder}/template/${file.dir.slice(templateDir.length)}/${file.base}`;
      templatesPaths.push(p);
      return p;
    },
  );

/*  let inputHtmlSize = 0;
  let outputHtmlSize = 0;
  for (let i = 0; i < templatesPaths.length; i++) {
    const p = templatesPaths[i];

    const f = await readFile(p);
    let res = '';
    try {
      res = await minify(f, {
        removeComments: true,
        collapseWhitespace: true,
        collapseInlineTagWhitespace: true,
        collapseBooleanAttributes: true,
        removeAttributeQuotes: true,
      });
    } catch (e) {
      inputHtmlSize += f.length
      outputHtmlSize += f.length
      // console.log('er', e);
      continue;
    }

    inputHtmlSize += f.length
    outputHtmlSize += res.length
    fs.writeFileSync(p, res);
  }

  console.log('HTML minifier:', `${inputHtmlSize}b`, '->', `${outputHtmlSize}b`);*/
}

function readFile(path) {
  return new Promise((res, rej) => {
    fs.readFile(path, 'utf8', function (err, data) {
      if (err) {
        return rej(err);
      }
      res(data);
    });
  });
}

function readJSON(path) {
  return new Promise((res, rej) => {
    fs.readFile(path, 'utf8', function (err, data) {
      if (err) {
        return rej(err);
      }
      res(JSON.parse(data));
    });
  });
}

module.exports = {
  init,
};
