class LoaderClass {

  countLoadings = 0;
  el: HTMLElement;
  menuBtnEl: HTMLElement;

  constructor() {
    this.el = document.querySelector('.loader-wrapper');
    this.menuBtnEl = document.querySelector('.menu__btn');

    this.el.innerHTML = `
      <div class="sk-cube-grid">
        ${new Array(9).fill(0).map((l, i) => `
            <div class="sk-cube sk-cube-${i + 1}"></div>
        `).join('')}
      </div>
    `;
  }

  add() {
    this.countLoadings++;
    this.check();
  }

  remove() {
    if (this.countLoadings === 0) {
      return;
    }

    this.countLoadings--;
    this.check();
  }

  check() {
    this.el.style.opacity = this.countLoadings > 0 ? '1' : '0';
    if (this.menuBtnEl) {
      this.menuBtnEl.style.opacity = this.countLoadings > 0 ? '0' : '1';
    }
  }

}

export const Loader = new LoaderClass();
