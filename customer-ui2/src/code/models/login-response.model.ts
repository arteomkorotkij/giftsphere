import {UserInfo} from "./user.model";

export interface LoginResponse {
    sessionId: string;
    user: UserInfo
}