export interface RouterModel {
    path: string,
    template: string,
    title: string,
    description: string,
    keywords: string,
}

export interface RouterMap {
    [key: string]: RouterModel;
}
