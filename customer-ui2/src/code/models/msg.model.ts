export class Msg<T> {
  status: number;
  msg: T;
}
