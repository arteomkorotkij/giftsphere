export interface UserInfo {
    cristal: number;
    email: string;
    facebookId: string;
    id: number;
    lang: string;
    lastConnected: number;
    lastDisconnected: number;
    money: number;
    name: string;
    role: number;
    totalTime: number;
}