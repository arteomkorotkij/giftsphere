import {CommonI} from "../typings";
import {HttpClient} from "./core";
declare var Common: CommonI;

class TranslatorClass {

    // { [key: lang]: Promise<translation> }
    private translations = new Map<string, Promise<{[key: string]: string}>>();

    async get(key: string, interpolate?: {[key: string]: any}, lang = Common.lang): Promise<string> {
        const translationMap = await this.getTranslatesMap(lang);
        let translation = translationMap[key];

        if (!translation) {
            translation = key;
        }

        return translation;
    }

    private getTranslatesMap(lang: string): Promise<{[key: string]: string}> {
        let translationProm = this.translations.get(lang);
        if (!translationProm) {
            translationProm = this.load(lang);
            this.translations.set(lang, translationProm);
        }

        return translationProm;
    }

    private async load(lang: string): Promise<{[key: string]: string}> {
        const lastVersion = Common.hasLangVersion[lang];
        const path = `/static/browser_i18n/${lang}.json`;
        const versionKey = `${path}_version`;
        const localVersion = localStorage.getItem(versionKey);
        const localValue = localStorage.getItem(path);

        if (localValue && lastVersion === localVersion) {
            return Promise.resolve(JSON.parse(localValue));
        }

        return await HttpClient.get(`${path}?v=${lastVersion}`, true).promise
            .then(data => {
                localStorage.setItem(versionKey, lastVersion);
                localStorage.setItem(path, JSON.stringify(data));
                return data;
            })
            .catch((err: any) => {
                alert(`Translator.load() error: ${err.msg}`);
                return {};
            });
    }

}

export const Translator = new TranslatorClass();
