export function sleep(duration = 0): Promise<void> {
    return new Promise(res => setTimeout(res, duration));
}
