import {CommonI} from "../typings";
import * as routers from "../interpolate/routers";
import {Translator} from "./translator";
declare var Common: CommonI;

class CoreClass {

    renderFn: (args: any) => void;
    rendered = new WSubject();
    loading = new WSubject();

    constructor() {
        Router.updateURL = this.updateURL.bind(this);
    }

    feedsNextPage({limit, page, templates, templatesStr}) {
        // let templateIds = 'pages/feeds-all-items.html';

        return this.updateSystemElement(HttpClient.get('/feeds/all?' + 'template_ids=' + templatesStr + '&limit=' + limit + '&page=' + page, true).promise, templates);
    }

    updateURL(state) {
        let templateIds = '';
        state.templates.forEach((tmpId, i) => {
            templateIds += tmpId;

            if (i < (state.templates.length - 1)) {
                templateIds += ',';
            }

            this.loading.next({tmpId})
        });

        return HttpClient.get('' + state.route + '?template_ids=' + templateIds + '&' + state.paramsStr, false).promise
            .then(d => this.convertServerSideRenderToHtmlMap(d))
            .then(htmlMap => {
                for (const field in htmlMap) {
                    const element = this.renderFn({systemName: field, newHtml: htmlMap[field]});
                    this.rendered.next({systemName: field, newHtml: htmlMap[field], element});
                }
            });
    }

    convertServerSideRenderToHtmlMap(data: string): {[key: string]: string} {
        const resp = {};

        const arr = data.split("\n\r\n\r");
        arr.forEach(row => {
            const i = row.indexOf("\n");
            resp[row.slice(0, i)] = row.slice(i+1);
        });

        return resp;
    }

    updateSystemElement(promise, systemNames) {
        systemNames.forEach(name => this.loading.next({systemName: name}));

        return promise.then(d => {
            for (const name in d) {
                const element = this.renderFn({systemName: name, newHtml: d[name]});
                this.rendered.next({systemName: name, newHtml: d[name], element});
            }
        })
    }

    setRenderFn(callback) {
        this.renderFn = callback;
    }

}

export interface CheckResponseI<T> {
    err: string,
    errEl: HTMLElement,
    errField: string,
    params: T,
    files: {[key: string]: File},
}

class ValidatorClass {

    validate = {
        NotEmpty: 1,
        IsString: 2,
        IsNumber: 3,
        CheckboxNumberArray: 4,
        CheckboxStringArray: 5,
        IsEmail: 6,
        RadioNumberGroup: 7,
        RadioStringGroup: 8,
        ParseToNumber: 9,
        Len(len) { return ['len', len] },
        MinLen(len) { return ['min-len', len] },
        MaxLen(len) { return ['max-len', len] },
        Min(min) { return ['min', min] },
        Max(max) { return ['max', max] },
        Func(callback) { return ['func', callback] },
    }

    constructor() {
    }

    check<T>({data, validate, isValidate, isNotValidate, skipSetErrToInput}: any): CheckResponseI<T> {
        let err = '';
        let el;
        let lastField = '';
        const params = {};
        const files = {};
        if (!validate) {
            validate = {};
        }
        if (!data) {
            data = {};
        }

        for (const field in data) {
            lastField = field;
            let value: any = '';
            let isFile = false;
            let isDate = false;
            const fieldData = data[field];
            if (!fieldData) {
                continue;
            }

            if (typeof(fieldData) === 'function') {
                value = fieldData();

            } else if (fieldData[0] === '#') {
                el = document.getElementById(fieldData.slice(1));
                if (!el) {
                    const msg = `Validator: Not found element by ${fieldData}`;
                    alert(msg);
                    throw msg;
                }

                if (el.type === 'checkbox') {
                    value = el.checked;
                } else if(el.type === 'number') {
                    value = parseFloat(el.value.replace(/,/g, '.'));
                } else if(el.type === 'file') {
                    value = el.files[0];
                    isFile = true;
                } else if(el.classList.contains('gfs-date-picker')) {
                    value = new Date(el.value);
                    isDate = true;
                } else {
                    value = el.value;
                }

            } else if (fieldData[0] === '.') {
                if (
                    validate[field] === this.validate.CheckboxNumberArray ||
                    validate[field] === this.validate.CheckboxStringArray
                ) {
                    value = [];

                    document.querySelectorAll(fieldData).forEach(checkbox => {
                        if (!checkbox.checked) {
                            return;
                        }

                        if (validate[field] === this.validate.CheckboxNumberArray) {
                            value.push(parseFloat(checkbox.getAttribute('let-value')));
                        } else if (validate[field] === this.validate.CheckboxStringArray) {
                            value.push(checkbox.getAttribute('let-value'));
                        }
                    });
                }

                if (
                    validate[field] === this.validate.RadioNumberGroup ||
                    validate[field] === this.validate.RadioStringGroup
                ) {
                    document.querySelectorAll(fieldData).forEach(checkbox => {
                        if (!checkbox.checked) {
                            return;
                        }

                        if (validate[field] === this.validate.RadioNumberGroup) {
                            value = parseFloat(checkbox.getAttribute('let-value'));
                        } else if (validate[field] === this.validate.RadioStringGroup) {
                            value = checkbox.getAttribute('let-value');
                        }
                    });
                }
                //
            } else {
                value = fieldData;
            }

            if (isFile) {
                files[field] = value;
            } else {
                params[field] = value;
            }

            let validatorStop;
            const valArr = validate ? validate[field] || [] : [];

            Array.isArray(valArr) && valArr.some(val => {
                validatorStop = val;

                if (val === this.validate.NotEmpty) {
                    if (
                        value === '' || value === undefined || value === null ||
                        (isDate && value.toString() === 'Invalid Date')
                    ) {
                        err = 'is empty';
                        return true;
                    }
                } else if (val === this.validate.IsNumber) {
                    if (!parseFloat(value) && !value.replace && value !== 0) {
                        err = `this is don't number`;
                        return true;
                    }
                } else if (val === this.validate.ParseToNumber) {
                    params[field] = value = parseFloat(value);
                } else if (val === this.validate.IsString) {
                    if (!value.replace) {
                        err = `this is don't string`;
                        return true;
                    }
                } else if (val === this.validate.IsEmail) {
                    params[field] = value = value.toLowerCase();

                    if (value != '' && !this.validateEmail(value)) {
                        err = `The e-mail is not correct`;
                        return true;
                    }
                } else {
                    if (val[0] === 'len') {
                        if (value.length !== val[1]) {
                            err = `length don't equal ` + val[1];
                            return true;
                        }
                    } else if (val[0] === 'max-len') {
                        if (value.length > val[1]) {
                            err = `length more ` + val[1];
                            return true;
                        }
                    } else if (val[0] === 'min-len') {
                        if (value.length < val[1]) {
                            err = `length less ` + val[1];
                            return true;
                        }
                    } else if (val[0] === 'min') {
                        if (value < val[1]) {
                            err = `less ` + val[1];
                            return true;
                        }
                    } else if (val[0] === 'max') {
                        if (value > val[1]) {
                            err = `more ` + val[1];
                            return true;
                        }
                    } else if (val[0] === 'func') {
                        err = val[1](field, value);
                        if (err !== '') {
                            return true;
                        }
                    }
                }
            });

            if (!skipSetErrToInput) {
                if (err !== '' && el) {
                    err = '"' + field + '" ' + err;
                    this.setValidate(el, isValidate, validatorStop, field, fieldData, value, err, true);
                    this.setValidate(el, isNotValidate, validatorStop, field, fieldData, value, err, false);
                    break;
                }

                el && this.setValidate(el, isNotValidate, validatorStop, field, fieldData, value, err, true);
                el && this.setValidate(el, isValidate, validatorStop, field, fieldData, value, err, false);
            }

            if (err) {
                break;
            }
        }

        let errEl;
        let errField;
        if (err) {
            errEl = el;
            errField = lastField;
        }

        return {err, errEl, errField, params: params as any, files};
    }

    validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    setValidate(copyEl, validate, validatorStop, field, fieldData, value, err, comeback) {
        if (validate) {
            if (Array.isArray(validate)) {
                if (validate[0][0] === '#') {
                    copyEl = document.getElementById(validate[0].slice(1));
                    validate = validate.splice(1)
                }

                validate.for(c => {
                    !comeback ? copyEl.classList.add(c) : copyEl.classList.remove(c);
                })
            } else if (typeof(validate) === 'function') {
                !comeback && validate(field, fieldData, value, err, validatorStop);
            } else {
                !comeback ? copyEl.classList.add(validate) : copyEl.classList.remove(validate);
            }
        }
    }

}

function stringify(params) {
    let str = '';

    for (const paramName in params) {
        if (params.hasOwnProperty(paramName)) {
            if (Object.prototype.toString.call(params[paramName]) === '[object Array]') {
                params[paramName].forEach(value => {
                    str += `${(str.length ? '&' : '') + paramName}=${encodeURIComponent(value)}`;
                });
            } else {
                str += `${(str.length ? '&' : '') + paramName}=${encodeURIComponent(params[paramName])}`;
            }
        }
    }

    return str;
}

function parseHTML(html) {
    const obj = {};

    let stop = false
    while (!stop) {
        const i1 = html.search(/\^\|/);
        const i2 = html.search(/\|\^/);
        const newHtml = html.slice(i1 + 2, i2)
        const arr = newHtml.split(`|->|`);

        if (!arr[0] || !arr[1]) {
            break;
        }

        obj[arr[0]] = arr[1];
        html = html.slice(i2 + 2)

        if (i1 === -1) {
            stop = true;
        }
    }

    return obj;
}

class HttpClientClass {

    constructor() {
    }

    get(url, parse?, events?) {
        return this.req('GET', url, parse, undefined, events);
    }

    post(url, body, parse?, events?) {
        return this.req('POST', url, parse, body, events);
    }

    put(url, body, parse?, events?) {
        return this.req('PUT', url, parse, body, events);
    }

    delete(url, parse?, events?) {
        return this.req('DELETE', url, parse, undefined, events);
    }

    req(method, url, parse, body, events) {
        let xhr = new XMLHttpRequest();

        // const state = new WSubject<number>(-1);
        const promise: Promise<any> = new Promise((res, rej) => {
            xhr.onreadystatechange = () => {
                // state.next(xhr.readyState);

                if (xhr.readyState !== 4) {
                    return;
                }

                if (xhr.status === 200 || xhr.status === 203) {
                    res(xhr.responseText);
                } else {
                    rej(xhr.responseText);
                }
            };

            events && events.uploadOnprogress && (xhr.upload.onprogress = events.uploadOnprogress)

            xhr.open(method, url, true);
            if (body && typeof body.append === "function") {
                xhr.send(body);
            } else {
                xhr.send(JSON.stringify(body));
            }
        }).then((data) => {
            if (parse === true) {
                try {
                    data = JSON.parse(data as string);
                } catch (e) {
                    data = parseHTML(data);
                }
            } else if (parse !== undefined && typeof parse !== 'boolean') {
                data = parseByObjJSON(JSON.parse(data as string), data);
            }

            return data;
        }).catch(err => {
            if (typeof err === 'string') {
                try {
                    err = JSON.parse(err as string);
                } catch (e) {
                    err = {msg: err};
                }
            }

            throw err;
        });

        return {
            promise,
            xhr,
        }
    }

}

class RouterClass {

    updateURL = async (state: any): Promise<void> => {};
    routerState: any;
    backTemplates: string[];
    skipConfirmExit = false;

    constructor() {
        const route = window.location.pathname;
        const params = this.getParams();
        let paramsStr = location.search.substring(1);
        this.routerState = {route, params, paramsStr, templates: ['router.html']};
        this.backTemplates = ['router.html'];

        window.addEventListener('popstate', () => {
            const state = window.history.state || Router.routerState;
            state.templates = this.backTemplates;
            this.updateURL(state);
        });
    }

    getRouterState() {
        return {... window.history.state ? window.history.state : this.routerState};
    }

    async go(path, params, merge, templates, skipRedirect?) {
        const confirmMsg = await confirmExit();
        if (confirmMsg) {
            return;
        }

        let lastURL = window.location.href;
        let rState = this.getRouterState();
        let lastParams = rState.params;

        if (merge) {
            for (const field in params) {
                lastParams[field] = params[field];
            }
        } else {
            lastParams = params;
        }

        let paramsStr = stringify(lastParams);

        window.history.pushState({route: path, params: lastParams, paramsStr: paramsStr, templates: templates}, 'path ' + path, path + (paramsStr ? '?' + paramsStr : ''));

        if (lastURL !== window.location.href) {
            return !skipRedirect ? this.updateURL(window.history.state) : null;
        }
    }

    getParams() {
        var s1 = location.search.substring(1, location.search.length).split('&'),
            r = {}, s2, i;

        for (i = 0; i < s1.length; i += 1) {
            s2 = s1[i].split('=');

            if (s2[0] !== '' && s2[1] !== '') {
                r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
            }
        }

        return r;
    }

    makePath(useLang: boolean, path: string, suffix: any = '') {
        return `${useLang ? `/${Common.lang}` : ``}${path}${suffix}`
    }

    back() {
        window.history.back();
    }

}

class WSubject {

    private _value: any;
    private subs = [];
    private startEmmit = false;

    get value() {
        return this._value;
    }

    constructor(v?) {
        this._value = undefined;

        if (v !== undefined) {
            this._value = v;
            this.startEmmit = true;
        }
    }

    subscribe(s) {
        this.subs.push(s);
        this.startEmmit && s(this.value);
        return
    }

    next(data) {
        this._value = data;
        this.subs.forEach(s => s(data));
    }
}

export function parseByObjJSON<T>(json: any, classModel: any): T {
    if (Array.isArray(json)) {
        for (const i in json) {
            json[i] = parseByObjJSON<T>(json[i], classModel);
        }

        return json as any;
    }

    return new classModel(json);
}

export const HttpClient = new HttpClientClass();
export const Router = new RouterClass();
window["Router"] = Router;
export const Validator = new ValidatorClass();
export const Core = new CoreClass();

window.onbeforeunload = confirmExit;
function confirmExit(): Promise<string> {
    if (Router.skipConfirmExit) {
        return null;
    }

    const pathname = window.location.pathname;
    const msg = Translator.get('BROWSER_DO_YOU_WANT_TO_LEAVE_PAGE_WITHOUT_SAVING');/*'Do you want to leave this page without saving?';*/
    const onlyPath = pathname.slice(pathname.indexOf('/', 1));

    // if (routers.coachEditProfile.path === onlyPath) {
    //     return msg;
    // } else if (routers.editProfile.path === onlyPath) {
    //     return msg;
    // } else if (routers.coachCourseNew.path === onlyPath) {
    //     return msg;
    // } else if (onlyPath.includes(routers.coachCourse.path)) {
    //     return msg;
    // }

    return null;
}
