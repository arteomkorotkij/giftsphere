import {HttpClient} from "./core";
import {SignupRequest} from "./models/signup-request.model";
import {LoginResponse} from "./models/login-response.model";
import {LoginRequest} from "./models/login-request.model";

export class DataSourceClass {

    ReqCreateUser(body: SignupRequest): Promise<LoginResponse> {
        return HttpClient.post(`/api/signup`, body, true).promise;
    }

    ReqLogin(body: LoginRequest): Promise<LoginResponse> {
        return HttpClient.post(`/api/login`, body, true).promise;
    }

    ReqLogout(): Promise<void> {
        return HttpClient.get('/api/logout').promise;
    }

}

export const DataSource = new DataSourceClass();