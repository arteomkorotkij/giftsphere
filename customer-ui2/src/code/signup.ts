import {HttpClient, Router, Validator} from "./core";
import {Translator} from "./translator";
import {Error} from "./models/autogen.models";
import {CommonI} from "../typings";
declare var Common: CommonI
import {Loader} from "./loader";
import * as routers from "../interpolate/routers";
import {DataSource} from "./data-source";
import {LoginRequest} from "./models/login-request.model";

class SignupClass {

    signupRequest = async () => {
        const errElMsg = document.getElementById('err-msg');
        errElMsg.style.display = 'none';

        const inputName = <HTMLInputElement>document.getElementById('signup-form-name')
        inputName.classList.remove('invalid');

        const inputEmail = <HTMLInputElement>document.getElementById('signup-form-email')
        inputEmail.classList.remove('invalid');

        const inputPassword = <HTMLInputElement>document.getElementById('signup-form-password')
        inputPassword.classList.remove('invalid');

        const inputConfirm = <HTMLInputElement>document.getElementById('signup-form-password-confirm')
        inputConfirm.classList.remove('invalid');


        let {err,errField, params} = Validator.check<any>({
            data: {
                email: '#signup-form-email',
                password: '#signup-form-password',
                name: '#signup-form-name',
            },
            validate: {
                email: [Validator.validate.NotEmpty, Validator.validate.IsEmail],
                password: [Validator.validate.NotEmpty, Validator.validate.MinLen(6)],
                name: [Validator.validate.NotEmpty],
            },
            skipSetErrToInput: true,
        });

        if (err === 'is empty') {
            if (errField === 'email') {
                err = await Translator.get('BROWSER_E-MAIL_IS_EMPTY');
                inputEmail.classList.add('invalid')
            } else if (errField === 'password') {
                err = await Translator.get('BROWSER_PASSWORD_IS_EMPTY');
                inputPassword.classList.add('invalid')
            }

            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;
        }

        if (err === 'The e-mail is not correct') {
            inputEmail.classList.add('invalid');
            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;
        }

        if (err === 'length less 6') {
            err = await Translator.get('BROWSER_PASSWORD_LENGTH_LESS_6');
            inputPassword.classList.add('invalid')
            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;
        }

        if (params.password !== inputConfirm.value) {
            err = await Translator.get('BROWSER_PASSWORD_DOES_NOT_MATCH');
            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;

        }

        Loader.add();
        const prom = DataSource.ReqCreateUser(params)
        prom.then((resp) => {
            location.href = Router.makePath(true, routers.index.path);
        });
        prom.catch((err: Error) => {
            if (err.msg) {
                errElMsg.innerText = err.msg;
                errElMsg.style.display = '';
            }
        });
        prom.finally(() => Loader.remove());
    };

}

export const Signup = new SignupClass();
window["Signup"] = Signup;