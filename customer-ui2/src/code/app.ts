import {Core, HttpClient, Router, Validator} from "./core";
import {CommonI} from "../typings";
import {Loader} from "./loader";
declare var Common: CommonI;

class AppClass {

    afterRenderSubs = [];
    afterRenderValue: any;
    defaultTemplates = ["router.html", "common/head.html"];
    constructor() {

    }


    afterRenderAdd(fn) {
        this.afterRenderSubs.push(fn);
        if (this.afterRenderValue !== undefined) {
            fn(...this.afterRenderValue);
        }
    }

    afterRenderEmit(value) {
        this.afterRenderValue = value;
        this.afterRenderSubs.forEach(fn => fn(...this.afterRenderValue));
    }

}

export const App = new AppClass();
window["App"] = App;

(function init() {
    const params = Router.getParams();

    function checkConfirmParams(key, eventData, queryData?: string) {
        if (!queryData) {
            queryData = params[key];
            if (!queryData) {
                return;
            }
        }

        const localDataStr = localStorage.getItem(key);
        let localData;

        if (localDataStr) {
            localData = JSON.parse(localDataStr);
        } else {
            localData = [];
        }

        if (localData.includes(queryData)) {
            return;
        }

        window["ga"] && window["ga"]("send", {
            hitType: "event",
            ...eventData,
        });

        localData.push(queryData);
        localStorage.setItem(key, JSON.stringify(localData));
    }

/*    checkConfirmParams("user-confirmed", {
        eventAction: "confirm_signup-user",
        eventCategory: "authorization",
    });
    checkConfirmParams("coach-confirmed", {
        eventAction: "confirm_signup-coach",
        eventCategory: "authorization",
    });*/

    const purchaseStripeKey = 'purchase-stripe-confirmed';
    const qPurchaseStripe: string = params[purchaseStripeKey];
    qPurchaseStripe && qPurchaseStripe.split(',').forEach((courseIdStr, i, arr) => {
        const courseId = parseInt(courseIdStr, 10);

        checkConfirmParams(purchaseStripeKey, {
            eventAction: `purchase-stripe${arr.length > 1 ? '-cart' : ''}`,
            eventCategory: "purchase",
            eventLabel: arr.length > 1 ? 'Purchased in cart' : 'Purchased from course page',
            eventValue: courseId,
        }, courseIdStr);
    });

})();

let feedsPage = 1;
let feedsLimit = 9;
let feedsAllowChange = true;

let landingSetEvent = false;
let animationBlocksList = {};
let animationBlocksListKeys = [];
const landingPageScroll = () => {
    // const scrollPos = window.pageYOffset || document.documentElement.scrollTop;
    animationBlocksListKeys.forEach(blockId => {
        if (animationBlocksList[blockId]) {
            return;
        }

        const blockEl = document.getElementById(blockId);
        if (!blockEl) {
            return;
        }
        const rect = blockEl.getBoundingClientRect();
        if (rect.top <= 450) {
            animationBlocksList[blockId] = true;
            blockEl.classList.remove('a-wait');
        }
    });
};

Core.setRenderFn(({ newHtml, systemName }) => {
    const element = document.querySelector(`[system="${systemName}"]`);

    switch (systemName) {
        case "pages/feeds-all-items.html": {
            element.insertAdjacentHTML('beforeend', newHtml);
            const text = newHtml.slice(48, 52);
            feedsAllowChange = text === "true";
            break;
        }
        default: {
            window.scrollTo(0, 0);
            element.innerHTML = newHtml;
            break;
        }
    }

    const menuToggle = (document.getElementById('menu__toggle') as HTMLInputElement);
    if (menuToggle) {
        menuToggle.checked = false;
    }
    const desktopMenu = document.getElementById('input-logged-block') as HTMLInputElement;
    desktopMenu && (desktopMenu.checked = false);
    updateSelectors(element);
    return element;
});

Router.backTemplates = ["router.html"];

let lastPath = "";
updateSelectors(document.body);
function updateSelectors(el) {
    const pathname = window.location.pathname;

    if (Common.analytics && lastPath !== pathname) {
        lastPath = pathname;
        window["ga"]("set", "page", pathname);
        window["ga"]("send", "pageview");
    }

/*    el.querySelectorAll('.gfs-date-picker').forEach(item => {
        new pikaday.default({
            numberOfMonths: 1,
            field: item,
            firstDay: 1,
            minDate: new Date(1960, 0, 1),
            maxDate: new Date(),
            yearRange: [1960, new Date().getFullYear() - 5]
        });
    })*/

    el.querySelectorAll(".gf-tooltip").forEach(item => {
        if (item.getAttribute('haveTooltip') === 'true' || !item.getAttribute('data-tooltip')) {
            return;
        }

        item.setAttribute('haveTooltip', 'true');
        const tooltip = document.createElement('div');
        tooltip.classList.add('tooltip-object');
        tooltip.innerText = item.getAttribute('data-tooltip');
        item.addEventListener('mouseover', (ev) => {
            const rect = ev.target.getBoundingClientRect();
            document.body.appendChild(tooltip);
            tooltip.style.top = `${window.pageYOffset + rect.top - 30}px`;
            tooltip.style.left = `${window.pageXOffset + rect.left + 10}px`;
        })
        item.addEventListener('mouseout', (ev) => {
            document.body.removeChild(tooltip);
        });
    });

    el.querySelectorAll(".router-controller").forEach( link => {
        const href = link.getAttribute("href") || "/";
        const arrObj = href.split("?");
        const path = arrObj[0];
        const merge = link.hasAttribute("merge");
        const templateAttr = link.getAttribute("template") || "";
        let templates = [... App.defaultTemplates];
        let params = arrObj[1] || "";
        let paramsArr = params.split("&");
        params = {};

        if (templateAttr) {
            templates = templateAttr.split(",");
        }

        paramsArr.forEach(v => {
            let vArr = v.split("=");
            if (vArr[0] && vArr[1]) {
                let value = vArr[1];

                params[vArr[0]] = value;
            }
        });

        link.onclick = (e) => {
            e.preventDefault();

            Loader.add();
            const prom = Router.go(path, params, merge, templates);
            if (prom) {
                prom.finally(() => Loader.remove());
            } else {
                Loader.remove();
            }
        };
    });

    el.querySelectorAll("[form-control]").forEach( form => {
        form.addEventListener("submit", (e) => e.preventDefault())
    } );

    changeSelectPage();

    const onlyPath = pathname.slice(pathname.indexOf('/', 1));
    if (onlyPath === "/") {
        if (!landingSetEvent) {
            animationBlocksList = {
                'preview-b': false,
                'b-1': false,
                'b-2': false,
                'b-3': false,
                'b-4': false,
                'b-5': false,
                // 'b-6': false,
                'b-7': false,
                'b-8': false,
            };
            animationBlocksListKeys = Object.keys(animationBlocksList);
            window.addEventListener("scroll", landingPageScroll);
            landingSetEvent = true;
            setTimeout(() => landingPageScroll(), 500);
        }
    } else {
        landingSetEvent = false;
        window.removeEventListener("scroll", landingPageScroll);
    }

    document.body.querySelectorAll('.lang-redirect').forEach((langEl: HTMLLinkElement) => {
        langEl.href = `/${langEl.lang}${onlyPath}`;
    });

    document.querySelectorAll('.hide-full-text').forEach((fullTextEl: HTMLElement) => {
        fullTextEl.setAttribute('title', fullTextEl.innerText);
    });
    App.afterRenderEmit([el, onlyPath]);
}

let headerIsOpen = false;
// regHeaderTogglerByEl(document.getElementById("btn-navbar-toggler"));
// regHeaderTogglerByEl(document.getElementById("btn-navbar-toggler-i"));
function regHeaderTogglerByEl(el) {
    el.addEventListener("click", function (e) {
        if (el !== e.srcElement) return;

        headerIsOpen = !headerIsOpen;
        if (headerIsOpen) {
            window.scrollTo({top: 0})
        }

        document.body.style.overflow = headerIsOpen ? "hidden" : "";
        document.getElementById("navbar-background").style.opacity = headerIsOpen ? "1" : "0";
        document.getElementById("main-body").style.filter = headerIsOpen ? "blur(3px)" : "";
    });
}

changeSelectPage();
function changeSelectPage() {
    const pathname = window.location.pathname;

    document.body.querySelectorAll("[m-system]").forEach( item => {
        const itemPathname = item.getAttribute("m-system");

        if (itemPathname === "/"+Common.lang+"/") {
            if (pathname === itemPathname) {
                item.classList.add("active");
            } else {
                item.classList.remove("active");
            }

            return
        }

        if (pathname.indexOf(itemPathname) === 0) {
            item.classList.add(item.getAttribute("m-class"));
        } else {
            item.classList.remove(item.getAttribute("m-class"));
        }
    });
}

