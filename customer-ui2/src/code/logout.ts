import {Loader} from "./loader";
import {DataSource} from "./data-source";
import {Router} from "./core";
import * as routers from "../interpolate/routers";

class LogoutClass {

    logout = async () => {
        Loader.add();
        await DataSource.ReqLogout();
        location.href = Router.makePath(true, routers.index.path);
        Loader.remove()
    }

}
export const Logout = new LogoutClass();
window["Logout"] = Logout;
