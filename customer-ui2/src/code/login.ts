import {Router, Validator} from "./core";
import {Translator} from "./translator";
import {Error} from "./models/autogen.models";
import {CommonI} from "../typings";
declare var Common: CommonI
import {Loader} from "./loader";
import * as routers from "../interpolate/routers";
import {DataSource} from "./data-source";

class LoginClass {

    login = async () => {
        const errElMsg = document.getElementById('err-msg');
        errElMsg.style.display = 'none';

        const inputEmail = <HTMLInputElement>document.getElementById('login-form-email')
        inputEmail.classList.remove('invalid');

        const inputPassword = <HTMLInputElement>document.getElementById('login-form-password')
        inputPassword.classList.remove('invalid');


        let {err,errField, params} = Validator.check<any>({
            data: {
                email: '#login-form-email',
                password: '#login-form-password',
            },
            validate: {
                email: [Validator.validate.NotEmpty, Validator.validate.IsEmail],
                password: [Validator.validate.NotEmpty, Validator.validate.MinLen(6)],
            },
            skipSetErrToInput: true,
        });

        if (err === 'is empty') {
            if (errField === 'email') {
                err = await Translator.get('BROWSER_E-MAIL_IS_EMPTY');
                inputEmail.classList.add('invalid')
            } else if (errField === 'password') {
                err = await Translator.get('BROWSER_PASSWORD_IS_EMPTY');
                inputPassword.classList.add('invalid')
            }

            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;
        }

        if (err === 'The e-mail is not correct') {
            inputEmail.classList.add('invalid');
            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;
        }

        if (err === 'length less 6') {
            err = await Translator.get('BROWSER_PASSWORD_LENGTH_LESS_6');
            inputPassword.classList.add('invalid')
            errElMsg.innerText = err;
            errElMsg.style.display = '';
            return;
        }

        Loader.add();
        const prom = DataSource.ReqLogin(params);
        prom.then((resp) => {
            location.href = Router.makePath(true, routers.index.path);
        });
        prom.catch((err: Error) => {
            if (err.msg) {
                errElMsg.innerText = err.msg;
                errElMsg.style.display = '';
            }
        });
        prom.finally(() => Loader.remove());
    };

}

export const Login = new LoginClass();
window["Login"] = Login;
