const rimraf = require("rimraf");
const fs = require("fs");
const interpolate = require('./src/interpolate/interpolate');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const crypto = require('crypto');


init().catch(err => console.error('error', err));
async function init() {
    rimraf.sync("dist");

    const { stdout, stderr } = await exec('parcel build src\\layout.html');
    if (stderr) {
        throw `${stderr}: ${stdout}`;
    }
    console.log(stdout);

    return interpolate.init(true);
}
