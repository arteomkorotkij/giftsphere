package tools

import (
	"time"

	"github.com/shopspring/decimal"
)

//var dayInMount = []int{
//	31,
//	28,
//	31,
//	30,
//	31,
//	30,
//	31,
//	31,
//	30,
//	31,
//	30,
//	31,
//}
//
//func GetDayInMount(numMount int) (count int) {
//	if numMount < 1 || numMount > 12 {
//		return -1
//	}
//	numMount--
//
//	count = dayInMount[numMount]
//	if numMount == 1 && (time.Now().Year() % 4) == 0 {
//		count++
//	}
//
//	return
//}

var secondsInMountDec = []decimal.Decimal{
	decimal.NewFromFloat(31 * 24 * 60 * 60), // 0
	decimal.NewFromFloat(28 * 24 * 60 * 60), // 1 может быть высокосный
	decimal.NewFromFloat(31 * 24 * 60 * 60),
	decimal.NewFromFloat(30 * 24 * 60 * 60),
	decimal.NewFromFloat(31 * 24 * 60 * 60),
	decimal.NewFromFloat(30 * 24 * 60 * 60),
	decimal.NewFromFloat(31 * 24 * 60 * 60),
	decimal.NewFromFloat(31 * 24 * 60 * 60),
	decimal.NewFromFloat(30 * 24 * 60 * 60),
	decimal.NewFromFloat(31 * 24 * 60 * 60),
	decimal.NewFromFloat(30 * 24 * 60 * 60),
	decimal.NewFromFloat(31 * 24 * 60 * 60), // 11
	//
	decimal.NewFromFloat(29 * 24 * 60 * 60), // 12 высокосный (1)
	decimal.NewFromFloat(-1),                // 13 не найден
}

func GetSecondsInMountDec(numMount int) decimal.Decimal {
	if numMount < 1 || numMount > 12 {
		return secondsInMountDec[13] // := -1
	}
	numMount--

	if numMount == 1 && (time.Now().Year()%4) == 0 { // если высокосный
		return secondsInMountDec[12] // 29
	}

	return secondsInMountDec[numMount]
}
