package tools

import "strings"

func IncludeStr(arr []string, search string) bool {
	for _, v := range arr {
		if v == search {
			return true
		}
	}

	return false
}

func IncludePrefixStr(text string, prefixs []string) bool {
	for _, prefix := range prefixs {
		if strings.HasPrefix(text, prefix) {
			return true
		}
	}

	return false
}

func ContainsOnly(text string, only ...string) bool {
	for _, item := range only {
		if strings.Contains(text, item) {
			return true
		}
	}

	return false
}

func JunctionInt16(arr, search []int16) bool {
	for _, arrValue := range arr {
		for _, searchValue := range search {
			if arrValue == searchValue {
				return true
			}
		}
	}
	return false
}
