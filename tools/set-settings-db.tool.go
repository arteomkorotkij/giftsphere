package tools

import (
	"giftsphere/db"
)

func SetSetting(DB db.ConnDB, key, value string) (err error) {
	_, err = DB.Exec(`
INSERT INTO settings ("key", "value") VALUES ($1, $2)
ON CONFLICT ("key")
DO
UPDATE SET "value"=$2;
`, key, value)

	return
}

func GetSetting(DB db.ConnDB, key string) (value string, err error) {
	err = DB.QueryRow(`SELECT value FROM settings WHERE "key"='` + key + `'`).Scan(&value)
	return
}

func DeleteSetting(DB db.ConnDB, key string) (err error) {
	_, err = DB.Exec(`DELETE FROM settings WHERE "key"='` + key + `'`)
	return
}
