package tools

import (
	"encoding/json"
	"fmt"
	"math"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/shopspring/decimal"
)

func PrettyPrint(v interface{}) {
	b, _ := json.MarshalIndent(v, "", "  ")
	fmt.Println(string(b))
}

// ToFixed - round number to selected precision
func ToFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(Round(num*output)) / output
}

// Round float number to int value
func Round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func EqualsOnly(text string, only ...string) bool {
	for _, item := range only {
		if text == item {
			return true
		}
	}

	return false
}

func BoolToStr(b bool, bTrue, bFalse string) string {
	if b {
		return bTrue
	} else {
		return bFalse
	}
}

func AwaitFn(fns ...func() error) <-chan error {
	ch := make(chan error, len(fns)+1)
	group := sync.WaitGroup{}
	group.Add(len(fns))

	for _, fn := range fns {
		go func(f func() error) {
			//defer func() {
			//	if pan := recover(); pan != nil {
			//		ch <- fmt.Errorf("AwaitFn() panica index func: %v stack:\n%v", i, pan)
			//	}
			//	group.Done()
			//}()

			err := f()
			if err != nil {
				ch <- err
			}
			group.Done()
		}(fn)
	}

	go func() {
		group.Wait()
		ch <- nil
	}()

	return ch
}

type CMDWriter struct {
	b []byte
}

func (c *CMDWriter) Write(p []byte) (n int, err error) {
	c.b = append(c.b, p...)
	return len(p), nil
}

func (c *CMDWriter) String() string {
	return string(c.b)
}

func Command(name string, arg ...string) (outStr string, err error) {
	cmd := exec.Command(name, arg...)
	var out CMDWriter
	var outErr CMDWriter

	cmd.Stdin = strings.NewReader("some input")
	cmd.Stdout = &out
	cmd.Stderr = &outErr

	err = cmd.Run()
	if err != nil {
		return
	}

	outErrStr := outErr.String()
	if outErrStr != "" {
		err = fmt.Errorf(outErrStr)
		return
	}

	outStr = out.String()
	return
}

func CutIfMore(text string, limit int) string {
	if len(text) > limit {
		text = text[:limit]
	}
	return text
}

func ConvertMinSecDuration(duration decimal.Decimal) (durationStr string) {
	dur, _ := duration.Float64()
	timeDur := time.Duration(dur) * time.Second

	m := timeDur.Minutes()
	minStr := strconv.Itoa(int(m))
	if len(minStr) == 1 {
		minStr = "0" + minStr
	}

	s := timeDur.Seconds()
	secStr := strconv.Itoa(int(s) - (int(m) * 60))
	if len(secStr) == 1 {
		secStr = "0" + secStr
	}

	return minStr + ":" + secStr
}

// IntStr - convert int64 to string
func IntStr(n int64) string {
	return strconv.Itoa(int(n))
}
