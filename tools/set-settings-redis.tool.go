package tools

import (
	"giftsphere/db"
)

func SetRedisSetting(DB db.ConnDB, key string, value []byte) (err error) {
	_, err = DB.Exec(`
INSERT INTO settings ("key", "value") VALUES ($1, $2)
ON CONFLICT ("key")
DO
UPDATE SET "value"=$2;
`, key, value)

	return
}

func GetRedisSetting(DB db.ConnDB, key string) (value []byte, err error) {
	err = DB.QueryRow(`SELECT value FROM settings WHERE "key"='` + key + `'`).Scan(&value)
	return
}

func DeleteRedisSetting(DB db.ConnDB, key string) (err error) {
	_, err = DB.Exec(`DELETE FROM settings WHERE "key"='` + key + `'`)
	return
}
