package web

import (
	_ "expvar"
	"fmt"
	"giftsphere/controllers/capi"
	"giftsphere/controllers/spherecontroller"
	"os"

	"giftsphere/conf"
	"giftsphere/controllers"
	"giftsphere/controllers/aapi"
	"giftsphere/controllers/cmvc"
	"giftsphere/router"
)

func Run() {
	customerHost := conf.Config.Str("ui_customer_host")

	// controller
	router.InitPublicController("/static/:*", customerHost, "", &controllers.StaticController)
	router.InitPublicController("/static", customerHost, "", &controllers.StaticController)

	// customer-mvc
	router.InitPublicControllerWithLang("/", customerHost, &cmvc.IndexController)
	router.InitPublicControllerWithLang("/login", customerHost, &cmvc.LoginController)
	router.InitPublicControllerWithLang("/signup", customerHost, &cmvc.SignUpController)
	router.InitPublicControllerWithLang("/spheres/:id", customerHost, &cmvc.SphereController)
	router.InitPublicControllerWithLang("/spheres", customerHost, &cmvc.SpheresListController)

	// api
	router.InitPublicControllerWithLang("/api/login", customerHost, &capi.LoginController)
	router.InitPublicControllerWithLang("/api/signup", customerHost, &capi.SignUpController)
	router.InitPublicControllerWithLang("/api/logout", customerHost, &capi.LogoutController)
	router.InitPublicControllerWithLang("/api/generate_test_data", customerHost, &spherecontroller.GenerateTestDataController)
	router.InitPublicControllerWithLang("/api/confirm", customerHost, &spherecontroller.СonfirmApiController)
	router.InitPublicControllerWithLang("/api/invitation/:inv", customerHost, &spherecontroller.InvitationApiController)
	router.InitPublicControllerWithLang("/api/driveout", customerHost, &spherecontroller.DriveOutApiController)
	router.InitPublicControllerWithLang("/api/sphere", customerHost, &spherecontroller.SphereApiController)

	// connect
	if conf.Config.Str("testing_web") == "on" || os.Getenv("getfitshapeDEBUGTEST") == "true" {
		ch := make(chan interface{})

		go func() {
			router.Run(conf.Config.Str("pub_port"), customerHost)
			ch <- nil
		}()

		<-ch
		return
	}

	webSite := router.Run(conf.Config.Str("pub_port"), customerHost)

	select {
	case <-webSite:
		fmt.Println("exiting")
	case <-aapi.ReloadChan:
		fmt.Println("reload")
	}
}
