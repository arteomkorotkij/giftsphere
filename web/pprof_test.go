package web_test

import (
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"giftsphere/conf"
	_ "giftsphere/init"
	"giftsphere/router"
)

func BenchmarkWeb(b *testing.B) {
	if os.Getenv("EXCLUSTERDEBUGTEST") != "true" && conf.Config.Str("testing_web") == "off" {
		fmt.Println("'EXCLUSTERDEBUGTEST' != true and 'testing_web' != on. web/pprof_test.go skiped")
		return
	}
	r := router.GetRouter()
	t1 := time.Now()

	respRec := httptest.NewRecorder()
	req, err := http.NewRequest("GET", conf.Config.Str("protocol")+"://localhost:"+conf.Config.Port("8080")+"/", nil)
	if err != nil {
		b.Fatal("Creating 'GET " + conf.Config.Str("protocol") + "://localhost:" + conf.Config.Port("8080") + "/' request failed!")
	}
	req.Header.Add("Accept-Encoding", "gzip, deflate, br")

	for n := 0; n < b.N; n++ {
		r.ServeHTTP(respRec, req)
		if respRec.Code != http.StatusOK {
			b.Fatal("Server error: Returned ", respRec.Code, " instead of ", http.StatusOK)
		}
		respRec.Flush()
	}

	dur := time.Since(t1)
	reqDur := dur.Seconds() / float64(b.N)
	reqInSec := math.Floor(1 / reqDur)

	file, err := os.Create("pprof.json")
	if err != nil {
		b.Fatal(err)
	}
	defer file.Close()
	_, _ = file.Write([]byte(`{
	"duration":"` + dur.String() + `",
	"requestsDuration":"` + fmt.Sprint(reqDur) + `s",
	"allRequests":"` + fmt.Sprint(b.N) + `",
	"countRequestsInSecond":"` + fmt.Sprint(reqInSec) + `"
}`))
}
