package web_test

import (
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"giftsphere/conf"
	_ "giftsphere/init"
	"giftsphere/router"
)

func BenchmarkRouter(b *testing.B) {
	if conf.Config.Str("testing_web") == "off" {
		fmt.Println("'testing_web' != on. web/web_test.go BenchmarkRouter() skiped")
		return
	}
	r := router.GetRouter()
	t1 := time.Now()

	respRec := httptest.NewRecorder()
	req, err := http.NewRequest("GET", conf.Config.Str("protocol")+"://localhost:"+conf.Config.Port("8080")+"/", nil)
	if err != nil {
		b.Fatal("Creating 'GET localhost:" + conf.Config.Port("8080") + "/' request failed!")
	}

	for n := 0; n < b.N; n++ {
		r.ServeHTTP(respRec, req)
		if respRec.Code != http.StatusOK {
			b.Fatal("Server error: Returned ", respRec.Code, " instead of ", http.StatusOK)
		}
		respRec.Flush()
	}

	if b.N > 100 {
		dur := time.Since(t1)
		reqDur := dur.Seconds() / float64(b.N)
		reqInSec := math.Floor(1 / reqDur)
		fmt.Print("Duration: ", dur, "     Req in second: ", reqInSec, "     ")
	}
}

func BenchmarkRouterGZip(b *testing.B) {
	if conf.Config.Str("testing_web") == "off" {
		fmt.Println("'testing_web' != on. web/web_test.go BenchmarkRouterGZip() skiped")
		return
	}
	r := router.GetRouter()
	t1 := time.Now()

	respRec := httptest.NewRecorder()
	req, err := http.NewRequest("GET", conf.Config.Str("protocol")+"://localhost:"+conf.Config.Port("8080")+"/", nil)
	if err != nil {
		b.Fatal("Creating 'GET localhost:" + conf.Config.Port("8080") + "/' request failed!")
	}
	req.Header.Add("Accept-Encoding", "gzip, deflate, br")

	for n := 0; n < b.N; n++ {
		r.ServeHTTP(respRec, req)
		if respRec.Code != http.StatusOK {
			b.Fatal("Server error: Returned ", respRec.Code, " instead of ", http.StatusOK)
		}
		respRec.Flush()
	}

	if b.N > 100 {
		dur := time.Since(t1)
		reqDur := dur.Seconds() / float64(b.N)
		reqInSec := math.Floor(1 / reqDur)
		fmt.Print("Duration: ", dur, "     Req in second: ", reqInSec, "     ")
	}
}
