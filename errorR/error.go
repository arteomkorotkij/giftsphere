//go:generate easyjson $GOFILE
package errorR

import (
	"strconv"

	"giftsphere/conf"
)

//easyjson:json
type Error struct {
	ID     int64  `json:"id"`
	Status int    `json:"status"`
	Msg    string `json:"msg"`
	Log    string `json:"-"`
}

func (e *Error) Error() string {
	return "ID: " + strconv.Itoa(int(e.ID)) + ". Status: " + strconv.Itoa(e.Status) + ". Msg: " + e.Log + ""
}

func (e *Error) Json() string {
	return `{"status":` + strconv.Itoa(e.Status) + `,"msg":"` + e.Msg + `","id":` + strconv.Itoa(int(e.ID)) + `}`
}

func NewError(status int, forWeb, forLog string, skipDB bool) *Error {
	if !conf.Testing && !skipDB { //nolint
		// TODO: put to table error_log DB
	}

	return &Error{
		ID:     0,
		Status: status,
		Msg:    forWeb,
		Log:    forLog,
	}
}
