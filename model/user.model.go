//go:generate easyjson $GOFILE
package model

import (
	"errors"
	"giftsphere/role"
	"strconv"
	"time"

	"github.com/stripe/stripe-go/v72"
)

//easyjson:json
type User struct {
	ID               int64     `json:"id"`
	Email            string    `json:"email"`
	FacebookID       string    `json:"facebookId"`
	Lang             string    `json:"lang"`
	Money            int64     `json:"money"`
	Cristal          int64     `json:"cristal"`
	LastConnected    int64     `json:"lastConnected"`
	LastDisconnected int64     `json:"lastDisconnected"`
	TotalTime        int64     `json:"totalTime"`
	Role             role.Role `json:"role"`
	Name             string    `json:"name"`
	Password         string    `json:"-"`
}

//easyjson:json
type UserList []*User

func (u *User) IsAuth() bool {
	return u.ID != 0
}

func (u *User) GetDefaultCountry() string {
	switch Lang(u.Lang) {
	case LangRu:
		return "Russia"
	case LangEs:
		return "Spain"
	case LangPt:
		return "Portuguese"
	case LangZh:
		return "US"
	case LangDe:
		return "US"
	}

	return "US"
}

func (u *User) StrID() string {
	return strconv.Itoa(int(u.ID))
}

func (u User) GetLang() Lang {
	if u.Lang != "" {
		return Lang(u.Lang)
	}

	return LangDefault
}

type SubscriptionEngine int8

const (
	SubscriptionEngineAppStore   SubscriptionEngine = 1
	SubscriptionEnginePlayMarket SubscriptionEngine = 2
)

var subscriptionEngineMap = map[string]SubscriptionEngine{
	"appstore":   SubscriptionEngineAppStore,
	"playmarket": SubscriptionEnginePlayMarket,
}

func SubscriptionEngineFromString(value string) (engine SubscriptionEngine, err error) {
	engine, has := subscriptionEngineMap[value]
	if !has {
		return 0, errors.New("I do not know such a subscription engine")
	}

	return
}

func (engine SubscriptionEngine) IsEngine() (result bool) {
	for _, value := range subscriptionEngineMap {
		if value == engine {
			return true
		}
	}

	return false
}

func (engine SubscriptionEngine) SubscriptionEngineToString() (result string) {
	for key, value := range subscriptionEngineMap {
		if value == engine {
			return key
		}
	}

	return ""
}

type Lang string

const LangDefault = LangEn

const (
	LangEn Lang = "en"
	LangRu Lang = "ru"
	LangEs Lang = "es"
	LangPt Lang = "pt"
	//
	LangDe Lang = "de"
	LangZh Lang = "zh"
)

var LangList = []Lang{
	LangEn,
	LangRu,
	//LangEs,
	//LangPt,
	//LangDe,
	//LangZh,
}

func SupportLang(lang Lang) bool {
	for _, l := range LangList {
		if l == lang {
			return true
		}
	}

	return false
}

func GetFullLang(lang string) string {
	switch Lang(lang) {
	case LangRu:
		return "Русский"
	case LangEs:
		return "Español"
	case LangZh:
		return "中文"
	case LangDe:
		return "Deutsch"
	case LangPt:
		return "Português"
	}

	return "English"
}

func DateToString(date time.Time, lang Lang) string {
	switch Lang(lang) {
	case LangRu:
		return date.Format("2006-01-02")
	case LangZh:
		return date.Format("2006/01/02")
	case LangDe, LangEs, LangPt:
		return date.Format("02/01/2006")
	}

	return date.Format("01/02/2006")
}

func CheckLang(lang string) string {
	switch Lang(lang) {
	case LangRu:
		return string(LangRu)
	case LangEs:
		return string(LangEs)
	case LangPt:
		return string(LangPt)
		//case LangZh:
		//	return string(LangZh)
		//case LangDe:
		//	return string(LangDe)
	}

	return string(LangDefault)
}

func ToZendeskLang(lang string) string {
	switch Lang(lang) {
	case LangRu:
		return "ru"
	case LangEs:
		return "es"
	case LangPt:
		return "pt"
	case LangZh:
		return "zh-cn"
	case LangDe:
		return "de"
	}

	return "en-us"
}

//easyjson:json

type StripeStatus int8

const (
	StripeStatusNo       StripeStatus = 0
	StripeStatusInactive StripeStatus = 1
	StripeStatusPending  StripeStatus = 2
	StripeStatusActive   StripeStatus = 30
)

func (s StripeStatus) String() string {
	switch s {
	case StripeStatusInactive:
		return string(stripe.AccountCapabilityStatusInactive)
	case StripeStatusPending:
		return string(stripe.AccountCapabilityStatusPending)
	case StripeStatusActive:
		return string(stripe.AccountCapabilityStatusActive)
	default:
		return ""
	}
}

type Gender int8

const (
	Male   Gender = 0
	Female Gender = 1
)

func (s Gender) String() string {
	switch s {
	case Male:
		return "male"
	case Female:
		return "female"
	}
	return "not found gender"
}
