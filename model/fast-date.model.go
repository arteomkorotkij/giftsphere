package model

import (
	"time"
)

type FastDate uint32

func (o *FastDate) UnmarshalJSON(data []byte) error {
	// Ignore null, like in the main JSON package.
	if string(data) == "null" {
		*o = 0
		return nil
	}

	// Fractional seconds are handled implicitly by Parse.
	t, err := time.Parse(`"`+time.RFC3339+`"`, string(data))
	if err != nil {
		t, err = time.Parse(`"2006-01-02"`, string(data))
		if err != nil {
			return err
		}
	}

	*o = FastDate(t.Unix())
	return err
}

func (o FastDate) MarshalJSON() ([]byte, error) {
	t := time.Unix(int64(o), 0)
	return t.MarshalJSON()
}

func (o FastDate) Unix() int64 {
	return int64(o)
}

func (o FastDate) Time() time.Time {
	return time.Unix(int64(o), 0)
}
