package model

import (
	"database/sql"
	"fmt"
	"giftsphere/db"
	"math"
	"strconv"
	"strings"
)

type GiftSphere struct {
	Id    int64     `json:"id"`
	Type  int64     `json:"type"`
	Level int64     `json:"level"`
	Place [15]int64 `json:"members"`
	Payed [8]bool   `json:"payed"`
}

type UserSpheres []UserSphere

type UserSphere struct {
	UserId   int64
	SphereId int64 `json:"id"`
	Type     int   `json:"type"`
	Level    int   `json:"level"`
}

type SphereTypes []SphereType

type SphereType struct {
	Type         int    `json:"type"`
	Level        int    `json:"level"`
	LevelName    string `json:"name"`
	CurrencyName string `json:"currency"`
}

func HaveUserSameSphere(id int64, sphereType, sphereLevel int, db db.ConnDB) (have bool, err error) {
	var rows *sql.Rows
	var spheres UserSpheres
	rows, err = db.Query(`SELECT sphere_id FROM user_spheres WHERE "user_id"='` + strconv.Itoa(int(id)) + `' AND "sphere_type""='` + strconv.Itoa(sphereType) + `' AND "sphere_level""='` + strconv.Itoa(int(sphereLevel)) + `'`)
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		var sphere UserSphere
		if err = rows.Scan(&sphere.SphereId); err != nil {
			return
		}
		spheres = append(spheres, sphere)
	}
	have = len(spheres) > 0
	return
}

func GetUserSpheres(id int64, db db.ConnDB) (spheres UserSpheres, err error) {
	var rows *sql.Rows
	rows, err = db.Query(`SELECT sphere_id, sphere_type, sphere_level FROM user_spheres WHERE "user_id"='` + strconv.Itoa(int(id)) + `'`)
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		var sphere UserSphere
		if err = rows.Scan(&sphere.SphereId, &sphere.Type, &sphere.Level); err != nil {
			return
		}
		spheres = append(spheres, sphere)
	}
	return
}

func AddUserSphere(us UserSphere, db db.ConnDB) (err error) {

	var recordID int64 = -2

	_ = db.QueryRow(`SELECT sphere_id FROM user_spheres WHERE "user_id"='` + strconv.Itoa(int(us.UserId)) + `' AND "sphere_type""='` + strconv.Itoa(us.Type) + `' AND "sphere_level""='` + strconv.Itoa(us.Level) + `'`).
		Scan(&recordID)
	if recordID != -2 {
		return fmt.Errorf("User are already in the same sphere!!! ")
	}
	_ = db.QueryRow(`SELECT sphere_type FROM user_spheres WHERE "user_id"='` + strconv.Itoa(int(us.UserId)) + `' AND "sphere_id""='` + strconv.Itoa(int(us.SphereId)) + `'`).
		Scan(&recordID)
	if recordID != -2 {
		return fmt.Errorf("User are already in this sphere!!! ")
	}

	_, err = db.Exec(`INSERT INTO user_spheres (user_id, sphere_type, sphere_level, sphere_id) VALUES ($1, $2, $3, $4)`,
		us.UserId, us.Type, us.Level, us.SphereId)

	return
}

func RemoveUserSphere(us UserSphere, db db.ConnDB) (err error) {
	_, err = db.Exec(`Delete FROM user_spheres WHERE "user_id"=$1 AND "sphere_type""=$2 AND "sphere_level""=$3 AND "sphere_id""=$4`, us.UserId, us.Type, us.Level, us.SphereId)
	return
}

func RemoveAllUserSpheres(sphereId int64, db db.ConnDB) (err error) {
	_, err = db.Exec(`Delete FROM user_spheres WHERE "sphere_id""=$1`, sphereId)
	return
}

func GetSphereTypes(dh *sql.DB) (types SphereTypes, err error) {
	var rows *sql.Rows
	rows, err = db.DB.Query(`SELECT sphere_type, sphere_level, sphere_level_name, sphere_currency_name FROM spheres_types`)
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		var tipe SphereType
		if err = rows.Scan(&tipe.Type, &tipe.Level, &tipe.LevelName, &tipe.CurrencyName); err != nil {
			return
		}
		types = append(types, tipe)
	}
	return
}

func GetSphere(id int64, db db.ConnDB) (sphere GiftSphere, err error) {
	payed := 0
	err = db.QueryRow(`SELECT id, sphere_type, sphere_level, place_00, place_01, place_02, place_03, place_04, place_05, place_06, place_07, place_08, place_09, place_10, place_11, place_12, place_13, place_14, payed FROM spheres WHERE "id"='`+strconv.Itoa(int(id))+`'`).
		Scan(&sphere.Id, &sphere.Type, &sphere.Level, &sphere.Place[0], &sphere.Place[1], &sphere.Place[2], &sphere.Place[3], &sphere.Place[4], &sphere.Place[5], &sphere.Place[6], &sphere.Place[7], &sphere.Place[8], &sphere.Place[9], &sphere.Place[10], &sphere.Place[11], &sphere.Place[12], &sphere.Place[13], &sphere.Place[14], &payed)
	if err != nil {
		return
	}
	sphere.Payed = [8]bool{
		(payed & 1) > 0,
		(payed & 2) > 0,
		(payed & 4) > 0,
		(payed & 8) > 0,
		(payed & 16) > 0,
		(payed & 32) > 0,
		(payed & 64) > 0,
		(payed & 128) > 0,
	}
	return
}

func (sphere *GiftSphere) PutInDatabase(db db.ConnDB) (err error) {
	payed := 0
	for i := range sphere.Payed {
		if sphere.Payed[i] {
			payed = payed + int(math.Pow(2, float64(i)))
		}
	}

	var recordID int64 = -2

	rows, err := db.Query(`SELECT id FROM spheres WHERE
		id=` + strconv.Itoa(int(sphere.Id)) + ";")
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&recordID)
		if err != nil {
			return
		}
	}

	if recordID == -2 {
		err = db.QueryRow(`INSERT INTO spheres (sphere_type, sphere_level, place_00, place_01, place_02, place_03, place_04, place_05, place_06, place_07, place_08, place_09, place_10, place_11, place_12, place_13, place_14, payed) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18) RETURNING id;`,
			sphere.Type, sphere.Level, sphere.Place[0], sphere.Place[1], sphere.Place[2], sphere.Place[3], sphere.Place[4], sphere.Place[5], sphere.Place[6], sphere.Place[7], sphere.Place[8], sphere.Place[9], sphere.Place[10], sphere.Place[11], sphere.Place[12], sphere.Place[13], sphere.Place[14], payed).
			Scan(&recordID)
		if err == nil {
			sphere.Id = recordID
		}
	} else {
		_, err = db.Exec(`UPDATE spheres SET sphere_type=$1, sphere_level=$2, place_00=$3, place_01=$4, place_02=$5, place_03=$6, place_04=$7, place_05=$8, place_06=$9, place_07=$10, place_08=$11, place_09=$12, place_10=$13, place_11=$14, place_12=$15, place_13=$16, place_14=$17, payed=$18 WHERE id=$19;`,
			sphere.Type, sphere.Level, sphere.Place[0], sphere.Place[1], sphere.Place[2], sphere.Place[3], sphere.Place[4], sphere.Place[5], sphere.Place[6], sphere.Place[7], sphere.Place[8], sphere.Place[9], sphere.Place[10], sphere.Place[11], sphere.Place[12], sphere.Place[13], sphere.Place[14], payed, recordID)
	}
	return
}

func (sphere *GiftSphere) RemoveSphere(db db.ConnDB) (err error) {
	_, err = db.Exec(`Delete FROM spheres WHERE "id""=$1`, sphere.Id)
	return
}

func (sphere *GiftSphere) CreateUserSpheres(db db.ConnDB) (err error) {
	query := "INSERT INTO user_spheres (user_id, sphere_type, sphere_level, sphere_id) VALUES "
	var inserts []string
	var params []interface{}
	for i := 8; i < 15; i++ {
		if sphere.Place[i] > 0 {
			leng := len(params)
			inserts = append(inserts, fmt.Sprintf("($%d, $%d, $%d, $%d)", leng+1, leng+2, leng+3, leng+4))
			params = append(params, sphere.Place[i], sphere.Type, sphere.Level, sphere.Id)
		}
	}
	queryVals := strings.Join(inserts, ",")
	query = query + queryVals

	_, err = db.Exec(query, params...)
	return
}

func (sphere *GiftSphere) GetUserPlace(userId int64) (place int) {
	place = -1
	for i := 0; i < 15; i++ {
		if sphere.Place[i] == userId {
			place = i
			break
		}
	}
	return
}
