package model

type StaticFile struct {
	Name         string `json:"name"`
	Data         []byte `json:"data"`
	ContentType  string `json:"contentType,omitempty"`
	CompressGzip []byte `json:"-"`
	CompressBr   []byte `json:"-"`
}
