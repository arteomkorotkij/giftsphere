package db

import (
	"context"
	"database/sql"
)

type Tx struct {
	Tx      *sql.Tx
	Stop    context.CancelFunc
	Context context.Context
}

func (ctx *Tx) Exec(query string, args ...interface{}) (res sql.Result, err error) {
	res, err = ctx.Tx.ExecContext(ctx.Context, query, args...)
	if err != nil {
		ctx.Stop()
	}
	return
}

func (ctx *Tx) Query(query string, args ...interface{}) (rows *sql.Rows, err error) {
	rows, err = ctx.Tx.QueryContext(ctx.Context, query, args...)
	if err != nil {
		ctx.Stop()
	}
	return
}

func (ctx *Tx) QueryRow(query string, args ...interface{}) RowI {
	row := ctx.Tx.QueryRowContext(ctx.Context, query, args...)
	return &Row{row, ctx.Stop}
}

func NewTx(context context.Context, stop context.CancelFunc) (*Tx, error) {
	tx, err := DB.BeginTx(context, nil)
	if err != nil {
		return nil, err
	}

	return &Tx{
		Tx:      tx,
		Context: context,
		Stop:    stop,
	}, nil
}

func NewTxWithCtx() (*Tx, context.Context, context.CancelFunc, error) {
	ctx, cancel := context.WithCancel(context.Background())
	tx, err := NewTx(ctx, cancel)
	return tx, ctx, cancel, err
}

type Row struct {
	row  *sql.Row
	stop context.CancelFunc
}

func (r *Row) Scan(dest ...interface{}) (err error) {
	err = r.row.Scan(dest...)
	if err != nil && r.stop != nil {
		r.stop()
	}
	return
}
