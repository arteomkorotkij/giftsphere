package db

import (
	"context"
	"database/sql"
)

type CtxDB struct {
	Stop    context.CancelFunc
	Context context.Context
}

func NewCtxDB() *CtxDB {
	c, s := context.WithCancel(context.TODO())
	return &CtxDB{
		Stop:    s,
		Context: c,
	}
}

func (ctx *CtxDB) Exec(query string, args ...interface{}) (res sql.Result, err error) {
	res, err = DB.ExecContext(ctx.Context, query, args...)
	if err != nil {
		ctx.Stop()
	}
	return
}

func (ctx *CtxDB) Query(query string, args ...interface{}) (rows *sql.Rows, err error) {
	rows, err = DB.QueryContext(ctx.Context, query, args...)
	if err != nil {
		ctx.Stop()
	}
	return
}

func (ctx *CtxDB) QueryRow(query string, args ...interface{}) RowI {
	row := DB.QueryRowContext(ctx.Context, query, args...)
	return &Row{row, ctx.Stop}
}

type RowI interface {
	Scan(dest ...interface{}) (err error)
}

type ConnDB interface {
	Exec(query string, args ...interface{}) (res sql.Result, err error)
	Query(query string, args ...interface{}) (rows *sql.Rows, err error)
	QueryRow(query string, args ...interface{}) RowI
}
