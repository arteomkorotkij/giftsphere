package db

import (
	"context"
	"fmt"
	"time"

	"github.com/garyburd/redigo/redis"

	"giftsphere/conf"
)

const (
	REDIS_SESSION_DB  = 0
	REDIS_COMMON_DB   = 1
	REDIS_USER_CHANEL = 2
)

// Errors
var (
	ErrIsEmptyRedisNetwork = fmt.Errorf("Can't param 'redis_network' in app.conf")
	ErrIsEmptyRedisAddress = fmt.Errorf("Can't param 'redis_address' in app.conf")
)

var (
	redisPool     *redis.Pool
	network       string
	address       string
	redisPassword string
	urlStr        string
)

func RunRedis() {
	if urlStr = conf.Config.Str("redis_url"); urlStr == "" {
		if network = conf.Config.Str("redis_network"); network == "" {
			panic(ErrIsEmptyRedisNetwork)
		}
		if address = conf.Config.Str("redis_address"); address == "" {
			panic(ErrIsEmptyRedisAddress)
		}
		redisPassword = conf.Config.Str("redis_pass")
	}

	redisPool = &redis.Pool{
		MaxIdle:     20,
		MaxActive:   20,
		IdleTimeout: time.Second * 10,
		Dial: func() (c redis.Conn, err error) {
			if urlStr != "" {
				c, err = redis.DialURL(urlStr)
			} else {
				params := []redis.DialOption{}
				if redisPassword != "" {
					params = append(params, redis.DialPassword(redisPassword))
				}

				c, err = redis.Dial(network, address, params...)
			}

			if err != nil {
				return nil, err
			}

			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}

			_, err := c.Do("PING")
			return err
		},
	}
}

func GetRedisPool() *redis.Pool {
	return redisPool
}

func GetRedisDatabase(dbNum int, ctx context.Context) (c redis.Conn, err error) {
	for i := 10000; i > 0; i-- {
		if ctx != nil {
			c, err = redisPool.GetContext(ctx)
		} else {
			c = redisPool.Get()
		}
		if err != nil {
			return
		}

		err = c.Err()
		if err == redis.ErrPoolExhausted {
			continue
		} else if err == nil {
			break
		}

		return
	}

	_, err = c.Do("SELECT", dbNum)
	return
}
