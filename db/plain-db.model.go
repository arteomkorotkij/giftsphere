package db

import (
	"database/sql"
)

var (
	PlainDB = &PDB{}
)

type PDB struct{}

func (ctx *PDB) Exec(query string, args ...interface{}) (res sql.Result, err error) {
	return DB.Exec(query, args...)
}

func (ctx *PDB) Query(query string, args ...interface{}) (rows *sql.Rows, err error) {
	return DB.Query(query, args...)
}

func (ctx *PDB) QueryRow(query string, args ...interface{}) RowI {
	row := DB.QueryRow(query, args...)
	return &Row{row, nil}
}
