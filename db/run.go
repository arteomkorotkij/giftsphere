package db

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/darkfoxs96/gomigrate/migration"
	_ "github.com/lib/pq"

	"giftsphere/conf"
	_ "giftsphere/db/migrate"
)

// Errors
var (
	ErrIsEmptyDBUsername = fmt.Errorf("Can't param 'db_username' in app.conf")
	ErrIsEmptyDBPassword = fmt.Errorf("Can't param 'db_password' in app.conf")
	ErrIsEmptyDBDatabase = fmt.Errorf("Can't param 'db_database' in app.conf")
	ErrIsEmptyDBHost     = fmt.Errorf("Can't param 'db_host' in app.conf")
	ErrIsEmptyDBPort     = fmt.Errorf("Can't param 'db_port' in app.conf")
)

var (
	DB       *sql.DB
	username string
	password string
	database string
	host     string
	port     string
	sslmode  string
	iterConn = 0
)

func Run() {
	if username = conf.Config.Str("db_username"); username == "" {
		panic(ErrIsEmptyDBUsername)
	}
	if password = conf.Config.Str("db_password"); password == "" {
		panic(ErrIsEmptyDBPassword)
	}
	if database = conf.Config.Str("db_database"); database == "" {
		panic(ErrIsEmptyDBDatabase)
	}
	if host = conf.Config.Str("db_host"); host == "" {
		panic(ErrIsEmptyDBHost)
	}
	if port = conf.Config.Str("db_port"); port == "" {
		panic(ErrIsEmptyDBPort)
	}
	if sslmode = conf.Config.Str("db_sslmode"); sslmode == "" {
		sslmode = "disable"
	}

	connectDB(username, password, database, host, port)

	err := migration.MigrateUp(DB, "ts")
	if err != nil {
		panic(err)
	}
}

func connectDB(login, pass, dbName, h, p string) {
	time.Sleep(5 * time.Second)
	var err error

	defer func() {
		if pan := recover(); pan != nil || err != nil {
			if iterConn < 10 {
				iterConn++
				connectDB(login, pass, dbName, h, p)
			} else {
				panic(fmt.Errorf("ERROR CONNECT TO DB: %v %v", pan, err))
			}
		}
	}()

	db, err := sql.Open("postgres", "user="+login+" password="+pass+" dbname="+dbName+" host="+h+" port="+p+" sslmode="+sslmode)
	if err != nil {
		return
	}

	err = db.Ping()
	if err != nil {
		return
	}

	fmt.Println("DB Connected.")
	DB = db
}
