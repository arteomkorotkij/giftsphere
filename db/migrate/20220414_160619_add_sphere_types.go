package migrate

import (
	"github.com/darkfoxs96/gomigrate/migration"
)

// DO NOT MODIFY
type Add_sphere_types_20220414_160619 struct {
	migration.Point
}

// DO NOT MODIFY
func init() {
	m := &Add_sphere_types_20220414_160619{}
	m.Key = "ts"
	migration.Register(1649941579, "20220414_160619", m)
}

// Run the migrations
func (m *Add_sphere_types_20220414_160619) Up() (err error) {
	_, err = m.DB.Exec(`
INSERT INTO spheres_types (sphere_type, sphere_level, sphere_level_name, sphere_currency_name) VALUES (0, 0, 'Bronze', '100$'),(0, 1, 'Silver', '400$'),(0, 2, 'Gold', '1600$'),(0, 3, 'Platinum', '5000$')
`)
	return
}

// Reverse the migrations
func (m *Add_sphere_types_20220414_160619) Down() (err error) {
	_, err = m.DB.Exec(`
`)
	return
}
