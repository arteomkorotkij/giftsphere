package migrate

import (
	"github.com/darkfoxs96/gomigrate/migration"
)

// DO NOT MODIFY
type Add_user_name_20220414_120300 struct {
	migration.Point
}

// DO NOT MODIFY
func init() {
	m := &Add_user_name_20220414_120300{}
	m.Key = "ts"
	migration.Register(1649926980, "20220414_120300", m)
}

// Run the migrations
func (m *Add_user_name_20220414_120300) Up() (err error) {
	_, err = m.DB.Exec(`
ALTER TABLE users ADD COLUMN "name" VARCHAR(255) NOT NULL DEFAULT '';
`)
	return
}

// Reverse the migrations
func (m *Add_user_name_20220414_120300) Down() (err error) {
	_, err = m.DB.Exec(`
ALTER TABLE users DROP COLUMN "name";
`)
	return
}
