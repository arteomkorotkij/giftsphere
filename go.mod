module giftsphere

go 1.17

require (
	github.com/andybalholm/brotli v1.0.4
	github.com/darkfoxs96/struct2ts v1.0.5
	github.com/garyburd/redigo v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/gobuffalo/logger v1.0.6 // indirect
	github.com/gobuffalo/packr/v2 v2.8.3
	github.com/gocraft/web v0.0.0-20190207150652-9707327fb69b
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/lib/pq v1.10.4
	github.com/mailru/easyjson v0.7.7
	github.com/shopspring/decimal v1.3.1
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stripe/stripe-go/v72 v72.100.0
	github.com/swaggo/http-swagger v1.2.5
	github.com/swaggo/swag v1.8.1
	github.com/tdewolff/minify v2.3.6+incompatible
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7
)

require github.com/darkfoxs96/gomigrate v1.0.7

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/OneOfOne/struct2ts v1.0.6 // indirect
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-openapi/swag v0.21.1 // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/gobuffalo/packd v1.0.1 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/markbates/errx v1.1.0 // indirect
	github.com/markbates/oncer v1.0.0 // indirect
	github.com/markbates/safe v1.0.1 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	github.com/tdewolff/test v1.0.6 // indirect
	golang.org/x/net v0.0.0-20220403103023-749bd193bc2b // indirect
	golang.org/x/tools v0.1.10 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
