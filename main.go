package main

import (
	_ "giftsphere/docs"
	_ "giftsphere/init"
	_ "github.com/darkfoxs96/struct2ts"
	_ "github.com/mailru/easyjson/gen"
)

//go:generate swag init
// @title FarmWebsite API
// @version 1.0
// @description This is a FarmWebsite server.
// @termsOfService http://swagger.io/terms/

// @contact.name FarmWebsite API Support
// @contact.url https://farm-website.com
// @contact.email darkfoxs96@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath /
// @query.collection.format multi

// @securityDefinitions.basic BasicAuth

// @securityDefinitions.apikey SessionID
// @in header
// @name Authorization

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name SwaggerAuthUserID
func main() {
}
