package conf

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

var (
	FileConfigPath    = "./app.conf"
	Config            = make(Conf)
	Testing           = false
	useProcessTimeLog = false
	devMode           = false
	Preview           = false
	Production        = false
	Version           = ""
)

func init() {
	Production = Version == "Production"

	path := os.Getenv("FARM_WEBSITE_CONFIG")
	if path != "" {
		FileConfigPath = path
	}
}

func IsDev() bool {
	return devMode
}

func UseProcessTimeLog() bool {
	return useProcessTimeLog
}

func Load() bool {
	_, err := ioutil.ReadFile(FileConfigPath)
	if err == nil {
		if err = loadConfig(); err != nil {
			fmt.Println("Error parsing config: " + err.Error())
			return false
		}
	} else {
		fmt.Println("Not found config")
	}

	Testing = Config.Str("testing") == "on"
	useProcessTimeLog = Config.Str("use_process_time_log") == "on"
	Preview = Config.Str("preview") == "on"
	devMode = Config.IsDev()
	return true
}

func loadConfig() (err error) {
	file, err := os.Open(FileConfigPath)
	if err != nil {
		return
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')

		// check if the line has = sign
		// and process the line. Ignore the rest.
		if equal := strings.Index(line, "="); equal >= 0 {
			if key := strings.TrimSpace(line[:equal]); len(key) > 0 {
				value := ""
				if len(line) > equal {
					value = strings.TrimSpace(line[equal+1:])
				}
				// assign the config map
				Config[key] = value
			}
		}
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
	}

	return
}
