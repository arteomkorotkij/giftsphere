package conf

import (
	"os"
	"strconv"
)

type Conf map[string]string

func (c *Conf) On(name string, start func()) {
	if c.getStr(name) == "on" {
		start()
	}
}

func (c *Conf) IsDev() bool {
	return c.getStr("dev") == "on"
}

func (c *Conf) Int(name string) (int, error) {
	return strconv.Atoi(c.getStr(name))
}

func (c *Conf) Str(name string) string {
	return c.getStr(name)
}

func (c *Conf) Bool(name string) bool {
	return c.getStr(name) == "true"
}

func (c *Conf) IsOn(name string) bool {
	return c.getStr(name) == "on"
}

func (c *Conf) Port(def string) string {
	port := c.getStr("port")
	if port == "" {
		port = def
	}

	return port
}

func (c *Conf) getStr(name string) string {
	v := (*c)[name]
	if v != "" {
		return v
	}

	return os.Getenv(name)
}
