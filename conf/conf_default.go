package conf

const DefaultConfig = `# Server
use_process_time_log = off
rsa_key              = ./rsa.key
http_errlog_path     = -
preview              = off
server_log           = ./homesport.log

# Testing
testing              = off
testing_web          = off
dev                  = on

# Network
protocol         = http
tls_use 		 = off
pub_port    	 = :8081
prv_port		 = :9099
rpc_port         = :9081
ui_customer_host = localhost:8081
domains          = localhost:8081||*

# DB
db_use 		  = on
db_username = homesport
db_password = homesport
db_database = homesport
db_host 	  = localhost
db_port 	  = 5432

# Redis
redis_use     = off
redis_network = tcp
redis_address = localhost:6379

# SendGrid
sendgrid_use     = off
sendgrid_api_key = no
sendgrid_email   = support@sporthome.com
sendgrid_name    = SportHome
sendgrid_temp_id = no

# VideoCloud
video_cloud_use      = on
video_cloud_api_key  = IGVss2OQHOBvYN7wz0kPGiERP9A9ZCRdNa013xTcIOu
video_cloud_sand_box = on

# PayPal
wallet_use       = on
wallet_client_id = AbMRoniiJ8tDBYiJ5jyuy2Nr2-gxQtQRHud73zIsLURHk_SvmbgDytdE7D8Y3cEWfZBrof0U-w2e-Ize
wallet_secret    = EERLBYI6kiAcJz4I0u2sicEue9f0KPHbR33P9YVvG9_FFhBYlFmuj62T-mdDbJ3CgIAWFpmxSDY-sPi_
wallet_sand_box  = on

# Cloud
cloud_use        = off
cloud_key_id     = cloud_key_id
cloud_key_secret = cloud_key_secret
cloud_endpoint   = cloud_endpoint
cloud_region     = us-east-1
cloud_bucket     = static.getfitshape
cloud_host       = https://static.getfitshape.com

# Fixer.io
fixer_use = off
fixer_key = fixer_key

# OAuthServices
oauth_secret_session_key = Secret-session-my-key

# Google
google_oauth_use         = on
google_oauth_key         = 908371149504-ikinhnr92kuui6glbsc7se5pn4j1jldq.apps.googleusercontent.com
google_oauth_secret      = llieGC3HhpqJjYXPMwBh98R7
google_oauth_callback    = -
google_recaptcha_key     = key
google_recaptcha_secret  = secret

# SearchEngine
search_engine_use  = off
search_engine_key  = masterKey
search_engine_host = http://127.0.0.1:7700

# Telegram
telegram_notification_bot_token = token
telegram_notification_chat_id = 0
`
