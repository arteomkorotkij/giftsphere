package init

import (
	"giftsphere/controllers/spherecontroller"
	"giftsphere/services/statisticsservice"
	"github.com/shopspring/decimal"

	"giftsphere/collections/usercollection"
	"giftsphere/conf"
	"giftsphere/controllers/controllermodel"
	"giftsphere/db"
	"giftsphere/router"
	"giftsphere/services/domainservice"
	"giftsphere/web"
)

// init
func init() {
	decimal.MarshalJSONWithoutQuotes = true

	if !conf.Load() {
		return
	}

	conf.Config.On("db_use", db.Run)
	conf.Config.On("redis_use", db.RunRedis)
	spherecontroller.LoadSpheresTypes()

	// Start services & collections
	if !conf.Testing {

		domainservice.Run()
		usercollection.Run()
		statisticsservice.Run()
		controllermodel.Run()

	}

	// Start router
	if !conf.Testing {
		router.InitRedirect()
		web.Run()
	}

}
