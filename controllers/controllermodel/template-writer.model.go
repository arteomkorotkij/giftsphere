package controllermodel

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"github.com/tdewolff/minify"
	minifyHtml "github.com/tdewolff/minify/html"

	"giftsphere/ectx"
	"giftsphere/router"
	"giftsphere/tools"
)

// Errors
var (
	ErrStop       = fmt.Errorf("stop")
	defaultMinify *minify.M
)

func init() {
	defaultMinify = minify.New()
	defaultMinify.AddFunc("text/html", minifyHtml.Minify)
}

type TemplateJsonWriter struct {
	ctx    *ectx.Context
	tmp    *template.Template
	ch     chan error
	tmpArr [][]byte
}

func (tjw *TemplateJsonWriter) WriteJSON(w *router.ResponseGZipWriter, data interface{}, templateIDs []string) (err error) {
	defer tjw.ctx.ProcessTimeLog.SetTimeWork("TemplateJsonWriter.WriteJSON()", time.Now())
	tjw.tmpArr = make([][]byte, len(templateIDs))
	tjw.ch = make(chan error, len(templateIDs)+1)

	for i := 0; i < len(templateIDs); i++ {
		tempID := templateIDs[i]
		index := i

		go func() {
			tw := TemplateWriter{Buf: []byte{}, ctx: tjw.ctx, TempID: tempID}
			err2 := tjw.tmp.ExecuteTemplate(&tw, tempID, data)
			if err2 != nil {
				tjw.ch <- err2
				return
			}

			html, err2 := defaultMinify.Bytes("text/html", tw.Buf)
			if err2 != nil {
				tjw.ch <- err2
				return
			}

			tjw.tmpArr[index] = append([]byte(tempID+"\n"), html...)
			tjw.ch <- nil
		}()
	}

	countDone := 0
	for {
		select {
		case err = <-tjw.ch:
			if err != nil {
				return err
			}

			countDone++
			if countDone == len(templateIDs) {
				//w.Header().Add("Content-Type", "application/json; charset=utf-8")
				w.WriteHeader(http.StatusOK)
				//_, _ = w.Write([]byte(`{`))

				for i := 0; i < len(tjw.tmpArr); i++ {
					t := tjw.tmpArr[i]
					_, _ = w.Write(t)

					if i != len(tjw.tmpArr)-1 {
						_, _ = w.Write([]byte("\n\r\n\r"))
					}
				}

				//_, _ = w.Write([]byte(`}`))
				return
			}
		case <-tjw.ctx.Context.Done():
			return tjw.ctx.Context.Err()
		}
	}
}

func (tjw *TemplateJsonWriter) WriteHTML(w *router.ResponseGZipWriter, data interface{}, templateID string) (err error) {
	w.Header().Add("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)

	tw := TemplateWriter{Buf: []byte{}, ctx: tjw.ctx, TempID: templateID}
	err = tjw.tmp.ExecuteTemplate(&tw, templateID, data)
	if err != nil {
		_, _ = w.Write([]byte(tjw.buildErrorHtml(string(tw.Buf), err.Error())))
		return nil
	}

	html, err := defaultMinify.Bytes("text/html", tw.Buf)
	if err != nil {
		_, _ = w.Write([]byte(tjw.buildErrorHtml(string(tw.Buf), err.Error())))
		return nil
	}

	_, _ = w.Write(html)
	return
}

func NewTemplateJsonWriter(ctx *ectx.Context, tmp *template.Template) *TemplateJsonWriter {
	return &TemplateJsonWriter{
		tmp: tmp,
		ctx: ctx,
	}
}

type TemplateWriter struct {
	Buf        []byte
	TempID     string
	ctx        *ectx.Context
	bytesCount int
}

func (tw *TemplateWriter) Write(p []byte) (n int, err error) {
	if err = tw.ctx.Context.Err(); err != nil {
		return 0, ErrStop
	}

	tw.bytesCount += len(p)
	if tw.bytesCount > 10000000 {
		err = tw.ctx.NewError(http.StatusInternalServerError, "template parsing error", "template parsing error: size out html > 10mb, tmpID '"+tw.TempID+"'.")
		return
	}

	tw.Buf = append(tw.Buf, p...)
	n = len(p)
	return
}

func (tjw *TemplateJsonWriter) buildErrorHtml(body, err string) string {
	return `
	<!DOCTYPE html>
<html lang="en" xmlns:router="http://www.w3.org/1999/xhtml">
<head>
    
</head>
<body>

<header>
</header>

<main class="w-100 h-100">
    <h1>Parsing template error:</h1>
	<h3>Body:</h1>
	<span><p id="html-body"></p><p style=color:red;>{{this}}</p></span>
	<h3>Error:</h1>
	<p>` + err + `</p>
</main>

<script>
	document.getElementById('html-body').innerText = ` + "`" + string(tools.JsonEscape(body)) + "`" + `;
	` + buildSystemScript(tjw.ctx) + `
</script>

</body>
</html>
`
}

func buildSystemScript(ctx *ectx.Context) string {
	if ctx.IsDevMode {
		return `
const socket = new WebSocket((window.location.protocol === 'http:' ? 'ws:' : 'wss:') + '//' + window.location.host + '/ws/dev?design_id=` + strconv.Itoa(int(ctx.DesignID)) + `');

socket.addEventListener('message', function (event) {
	if (event.data === 'update') {
	  document.location.reload(true);
	}
});

window.onbeforeunload = function (e) {
  	socket.send('close');
};
`
	} else {
		return ""
	}
}
