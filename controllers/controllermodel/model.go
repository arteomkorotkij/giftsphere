//go:generate easyjson $GOFILE
package controllermodel

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"github.com/gocraft/web"
	"golang.org/x/text/language"

	"giftsphere/conf"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"

	"giftsphere/services/domainservice"
	"giftsphere/services/staticservice"
	"giftsphere/services/statisticsservice"
	"giftsphere/session"
)

// Errors
var (
	ErrNotFoundLang = fmt.Errorf("Not found translate")
)

//easyjson:json
type Msg struct {
	Status bool        `json:"status"`
	Msg    interface{} `json:"msg"`
}

type LangItem struct {
	Href template.URL
	Name string
	Lang string
}

type DataMVC struct {
	Breadcrumb         []string           `json:"breadcrumb"`
	I18N               staticservice.I18N `json:"i18n"`
	Lang               string             `json:"lang"`
	ZendeskLang        string             `json:"zendeskLang"`
	FullLang           string             `json:"fullLang"`
	IsDevMode          bool               `json:"isDevMode"`
	Path               string             `json:"path"`
	CaptchaID          string             `json:"captchaId"`
	Analytics          bool               `json:"analytics"`
	AnalyticsKey       string             `json:"analyticsKey"`
	CountInCart        int32              `json:"countInCart"`
	HaveSession        bool               `json:"haveSession"`
	IsAuth             bool               `json:"isAuth"`
	IsCoach            bool               `json:"isCouch"`
	Chat               bool               `json:"chat"`
	ChatAuth           bool               `json:"chatAuth"`
	ChatUrl            string             `json:"chatUrl"`
	ChatJWT            string             `json:"chatJWT"`
	PaypalClientID     string             `json:"paypalClientID"`
	StripePublicKey    string             `json:"stripeClientID"`
	GoogleReCaptchaKey string             `json:"googleReCaptchaKey"`
	AssemblyPayments   bool               `json:"assemblyPayments"`
	URL                string             `json:"url"`
	Name               string             `json:"fullName"`
	UserID             int64              `json:"userId"`
	UserEmail          string             `json:"userEmail"`
	User               *model.User        `json:"user"`
	OtherDomains       template.JS        `json:"otherDomains"`
	LangList           []*LangItem
}

var NotFullPathLangList []*LangItem

var matcher = language.NewMatcher([]language.Tag{
	language.English,
	language.Russian,
	//language.Spanish,
	//language.Portuguese,
	//language.German,
	//language.Chinese,
})

func Run() {
	schema := conf.Config.Str("protocol")
	for _, l := range model.LangList {
		lStr := string(l)
		domain := domainservice.GetDomainByLang(lStr).Domain

		NotFullPathLangList = append(NotFullPathLangList, &LangItem{
			Href: template.URL(schema + "://" + domain + "/" + lStr),
			Name: model.GetFullLang(lStr),
			Lang: lStr,
		})
	}
}

const QUERY_REF_NAME = "ref"

func NewDataMVC(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) (d *DataMVC, err error) {
	d = &DataMVC{}
	d.Lang = ctx.Lang
	d.IsDevMode = ctx.IsDevMode
	d.Path = r.URL.Path
	d.HaveSession = ctx.Session.SessID != ""
	d.IsAuth = ctx.Session.UserID != 0
	d.Name = ctx.Session.User.Name
	d.UserEmail = ctx.Session.User.Email
	d.UserID = ctx.Session.User.ID
	d.User = ctx.Session.User
	d.URL = r.URL.String()

	d.AnalyticsKey = statisticsservice.GetAnalyticsKey()
	if d.AnalyticsKey != "" {
		d.Analytics = true
	}

	query := r.URL.Query()
	ref := query.Get(QUERY_REF_NAME)
	if ref != "" {
		expiration := time.Now().Add(session.RedisUserExpirationTime * time.Second)
		cookie := http.Cookie{Name: QUERY_REF_NAME, Value: ref, Expires: expiration, Path: "/"}
		http.SetCookie(w, &cookie)
		query.Del(QUERY_REF_NAME)
	}

	if ctx.PathLang == "" {
		queryEncode := query.Encode()
		if queryEncode != "" {
			queryEncode = "?" + queryEncode
		}

		if d.Lang == "" && ctx.Session.User.Lang != "" {
			d.Lang = ctx.Session.User.Lang
		} else if d.Lang == "" {
			d.Lang = GetLangFromBrowser(r.Request)
		}

		schema := conf.Config.Str("protocol")
		http.Redirect(w, r.Request, schema+"://"+domainservice.GetDomainByLang(d.Lang).Domain+"/"+d.Lang+r.URL.Path+queryEncode, http.StatusFound)
		return nil, ectx.ErrSkip
	}

	domain := getDomain(r)
	if domain != domainservice.GetDomainByLang(d.Lang).Domain {
		queryEncode := query.Encode()
		if queryEncode != "" {
			queryEncode = "?" + queryEncode
		}

		schema := conf.Config.Str("protocol")
		http.Redirect(w, r.Request, schema+"://"+domainservice.GetDomainByLang(d.Lang).Domain+r.URL.Path+queryEncode, http.StatusFound)
		return nil, ectx.ErrSkip
	}

	d.FullLang = model.GetFullLang(d.Lang)
	d.ZendeskLang = model.ToZendeskLang(d.Lang)

	found := false
	for i, v := range d.Path {
		if v == '/' {
			if found {
				d.Path = d.Path[i:]
				break
			} else {
				found = true
			}
		}
	}

	d.I18N = staticservice.GetI18N(ctx.Lang)
	if d.I18N == nil {
		err = ErrNotFoundLang
		return
	}

	for _, item := range NotFullPathLangList {
		d.LangList = append(d.LangList, &LangItem{
			Href: item.Href + template.URL(d.Path),
			Name: item.Name,
			Lang: item.Lang,
		})
	}

	for _, dc := range domainservice.GetDomainControls() {
		if dc.Domain != domain {
			if d.OtherDomains != "" {
				d.OtherDomains += ","
			}
			d.OtherDomains += `'` + template.JS(dc.Domain) + `'`
		}
	}

	return
}

func getDomain(r *web.Request) string {
	originHost := r.Header.Get("OriginHost")
	if originHost != "" {
		return originHost
	}

	return domainservice.GetDomainByLang("*").Domain
}

func GetLangFromBrowser(r *http.Request) string {
	lang, _ := r.Cookie("lang")
	accept := r.Header.Get("Accept-Language")
	tag, _ := language.MatchStrings(matcher, lang.String(), accept)
	base, _ := tag.Base()
	return model.CheckLang(base.String())
}
