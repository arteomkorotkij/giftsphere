package controllermodel

import (
	"fmt"
	"net/http"

	"github.com/gocraft/web"

	"giftsphere/ectx"
	"giftsphere/router"
	"giftsphere/services/staticservice"
)

type Controller struct {
}

func (c *Controller) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	_, _ = w.Write([]byte(`{"status":405,"msg":"method not allowed"}`))
}

func (c *Controller) Post(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	_, _ = w.Write([]byte(`{"status":405,"msg":"method not allowed"}`))
}

func (c *Controller) Delete(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	_, _ = w.Write([]byte(`{"status":405,"msg":"method not allowed"}`))
}

func (c *Controller) Put(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	_, _ = w.Write([]byte(`{"status":405,"msg":"method not allowed"}`))
}

func (c *Controller) WriteTemplate(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request, data interface{}) (err error) {
	templateIDs, _ := ctx.Parameters.GetStringArr("template_ids")
	tmpEx := staticservice.GetTmp()
	if tmpEx == nil {
		return fmt.Errorf("Not found template")
	}

	if len(templateIDs) == 0 {
		err = NewTemplateJsonWriter(ctx, tmpEx).WriteHTML(w, data, "layout.html")
	} else {
		err = NewTemplateJsonWriter(ctx, tmpEx).WriteJSON(w, data, templateIDs)
	}

	return ectx.CheckError(ctx, err, http.StatusInternalServerError, "template parsing error")
}
