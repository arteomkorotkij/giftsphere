package controllers

import (
	"fmt"
	"strconv"
	"time"

	"github.com/gocraft/web"

	"giftsphere/conf"
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/router"
	"giftsphere/services/staticservice"
)

type StaticC struct{ controllermodel.Controller }

var StaticController = StaticC{}
var cacheExpire = time.Hour * 24 * 7 // 7 days

func (c *StaticC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	fileName, err := ctx.Parameters.GetString("path")
	if ctx.CheckError(w, r, err) {
		return
	}

	file := staticservice.GetStatic(fileName)
	if file == nil {
		ctx.WriteJson(w, r, fmt.Errorf("Not found file"), nil)
		return
	}

	if !conf.IsDev() {
		t := time.Now().Add(cacheExpire)
		w.Header().Add("cache-control", "public, max-age="+strconv.Itoa(int(cacheExpire/time.Second)))
		w.Header().Add("expires", t.Format(time.RFC1123))
		w.Header().Add("x-cache", "HIT")
	}

	if w.AcceptsBrotli && len(file.CompressBr) > 0 {
		w.NoCompression()
		w.Header().Add("Content-Encoding", "br")
		w.Header().Add("Vary", "Accept-Encoding")
		w.Header().Add("Content-Type", file.ContentType)
		w.Header().Add("Content-Length", strconv.Itoa(len(file.CompressBr)))
		if _, err = w.Write(file.CompressBr); err != nil {
			fmt.Println("StaticController", err)
		}
	} else if w.AcceptsGzip && len(file.CompressGzip) > 0 {
		w.NoCompression()
		w.Header().Add("Content-Encoding", "gzip")
		w.Header().Add("Vary", "Accept-Encoding")
		w.Header().Add("Content-Type", file.ContentType)
		w.Header().Add("Content-Length", strconv.Itoa(len(file.CompressGzip)))
		if _, err = w.Write(file.CompressGzip); err != nil {
			fmt.Println("StaticController", err)
		}
	} else {
		if _, err = w.Write(file.Data); err != nil {
			fmt.Println("StaticController", err)
		}
	}
}
