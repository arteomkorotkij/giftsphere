package cmvc

import (
	"html/template"

	"github.com/gocraft/web"

	"giftsphere/collections/usercollection"
	"giftsphere/controllers/controllermodel"
	"giftsphere/controllers/spherecontroller"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
)

type SphereC struct{ controllermodel.Controller }

type CircleItem struct {
	UserName    string
	CX          string
	CY          string
	FilledStyle template.CSS
	Index       int
	IsPayed     bool
}

type Sphere struct {
	Id          int64
	TypeName    string
	Cost        string
	Items       [15]CircleItem
	IsUserPayed [8]bool
}

type SphereData struct {
	*controllermodel.DataMVC
	Sphere Sphere
}

var SphereController = SphereC{}

func (c *SphereC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {

	sphereId, err := ctx.Parameters.GetInt64("id")
	sphere, err := model.GetSphere(sphereId, ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}

	d, err := controllermodel.NewDataMVC(ctx, w, r)
	if err == controllermodel.ErrNotFoundLang {
		ctx.Parameters.Set("path", r.URL.Path[1:])
		//controllers.CustomDataController.Get(ctx, w, r)
		return
	}

	items := [15]CircleItem{}
	for i, user := range sphere.Place {
		var name string
		ud, err := usercollection.GetUserByID(user, ctx.DB)
		if err != nil {
			name = "Error!!!"
		} else {
			name = ud.Name
		}
		items[i].UserName = name
		items[i].CY = circleItemDefaults[i].CY
		items[i].CX = circleItemDefaults[i].CX
		items[i].Index = circleItemDefaults[i].Index
		items[i].FilledStyle = circleItemDefaults[i].FilledStyle

		if i < 8 {
			items[i].IsPayed = sphere.Payed[i]
		} else {
			items[i].IsPayed = true
		}
	}

	levelName := ""
	cost := ""
	tip := spherecontroller.GetSpheresTypes()
	if tp, OK := tip[int(sphere.Type)]; OK {
		if lv, OK := tp[int(sphere.Level)]; OK {
			cost = lv.CurrencyName
			levelName = lv.LevelName + " Sphere"
		}
	}

	if ctx.CheckError(w, r, err) {
		return
	}

	data := SphereData{
		DataMVC: d,
		Sphere: Sphere{
			Id:          sphere.Id,
			TypeName:    levelName,
			Cost:        cost,
			Items:       items,
			IsUserPayed: sphere.Payed,
		},
	}

	if err = c.WriteTemplate(ctx, w, r, data); err != nil {
		ctx.WriteJson(w, r, err, nil)
	}
}

var circleItemDefaults = [15]CircleItem{
	{
		CX:          "109.99994",
		CY:          "86.999962",
		Index:       0,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "180.62189",
		CY:          "116.37798",
		Index:       1,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "209.99995",
		CY:          "187.00002",
		Index:       2,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "180.62164",
		CY:          "257.65857",
		Index:       3,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "109.99994",
		CY:          "287",
		Index:       4,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "39.37817",
		CY:          "257.65857",
		Index:       5,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "10",
		CY:          "187.00002",
		Index:       6,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "39.37817",
		CY:          "116.59304",
		Index:       7,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "109.99994",
		CY:          "246.99994",
		Index:       8,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "169.99995",
		CY:          "187.00002",
		Index:       9,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "49.999966",
		CY:          "187.00002",
		Index:       10,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "109.99994",
		CY:          "126.99991",
		Index:       11,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "139.99995",
		CY:          "216.99997",
		Index:       12,
		FilledStyle: "fill:#00aaff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "79.999901",
		CY:          "156.99991",
		Index:       13,
		FilledStyle: "fill:#aa00ff;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
	{
		CX:          "109.99994",
		CY:          "187.00002",
		Index:       14,
		FilledStyle: "fill:#ffc500;fill-opacity:1;stroke:#000000;stroke-width:0.26112881;stroke-opacity:1",
	},
}
