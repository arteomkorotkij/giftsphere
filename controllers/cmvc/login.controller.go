package cmvc

import (
	"github.com/gocraft/web"

	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/router"
)

type LoginC struct{ controllermodel.Controller }

type LoginData struct {
	*controllermodel.DataMVC
}

var LoginController = LoginC{}

func (c *LoginC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	d, err := controllermodel.NewDataMVC(ctx, w, r)
	if ctx.CheckError(w, r, err) {
		return
	}

	data := LoginData{
		DataMVC: d,
	}

	if err = c.WriteTemplate(ctx, w, r, data); err != nil {
		ctx.WriteJson(w, r, err, nil)
	}
}
