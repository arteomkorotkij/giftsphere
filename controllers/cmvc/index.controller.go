package cmvc

import (
	"github.com/gocraft/web"

	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/router"
)

type IndexC struct{ controllermodel.Controller }

type IndexData struct {
	*controllermodel.DataMVC
}

var IndexController = IndexC{}

func (c *IndexC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	d, err := controllermodel.NewDataMVC(ctx, w, r)
	if err == controllermodel.ErrNotFoundLang {
		ctx.Parameters.Set("path", r.URL.Path[1:])
		//controllers.CustomDataController.Get(ctx, w, r)
		return
	}

	if ctx.CheckError(w, r, err) {
		return
	}

	data := IndexData{
		DataMVC: d,
	}

	if err = c.WriteTemplate(ctx, w, r, data); err != nil {
		ctx.WriteJson(w, r, err, nil)
	}
}
