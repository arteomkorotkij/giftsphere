package cmvc

import (
	"github.com/gocraft/web"

	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/router"
)

type SignUpC struct{ controllermodel.Controller }

type SignUpData struct {
	*controllermodel.DataMVC
}

var SignUpController = SignUpC{}

func (c *SignUpC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	d, err := controllermodel.NewDataMVC(ctx, w, r)
	if ctx.CheckError(w, r, err) {
		return
	}

	data := SignUpData{
		DataMVC: d,
	}

	if err = c.WriteTemplate(ctx, w, r, data); err != nil {
		ctx.WriteJson(w, r, err, nil)
	}
}
