package cmvc

import (
	"giftsphere/controllers/controllermodel"
	"giftsphere/controllers/spherecontroller"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"github.com/gocraft/web"
)

type SpheresListC struct{ controllermodel.Controller }

type SphereTile struct {
	Id   int64
	Name string
}

type SpheresData struct {
	*controllermodel.DataMVC
	Spheres []SphereTile
}

var SpheresListController = SpheresListC{}

func (c *SpheresListC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {

	d, err := controllermodel.NewDataMVC(ctx, w, r)
	if err == controllermodel.ErrNotFoundLang {
		ctx.Parameters.Set("path", r.URL.Path[1:])
		//controllers.CustomDataController.Get(ctx, w, r)
		return
	}

	if ctx.CheckError(w, r, err) {
		return
	}

	list, err := model.GetUserSpheres(ctx.Session.UserID, ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}

	tip := spherecontroller.GetSpheresTypes()
	var sphs []SphereTile
	for _, sph := range list {
		if tp, OK := tip[sph.Type]; OK {
			if lv, OK := tp[sph.Level]; OK {
				sphs = append(sphs, SphereTile{
					sph.SphereId,
					lv.LevelName + " Sphere",
				})
			}
		}
	}

	data := SpheresData{
		DataMVC: d,
		Spheres: sphs,
	}

	if err = c.WriteTemplate(ctx, w, r, data); err != nil {
		ctx.WriteJson(w, r, err, nil)
	}
}
