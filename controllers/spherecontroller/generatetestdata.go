package spherecontroller

import (
	"fmt"
	"giftsphere/collections/usercollection"
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"github.com/gocraft/web"
	"golang.org/x/crypto/bcrypt"
)

type GenerateTestDataC struct{ controllermodel.Controller }

var GenerateTestDataController = GenerateTestDataC{}
var errorAlready = fmt.Errorf("Test data is Already feed!!! ")

// Get godoc
// @Summary Confirm member payment
// @Tags User
// @Produce json
// @Success 200 {object} "OK"
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/generate_test_data [get]
func (c *GenerateTestDataC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {

	total, err := usercollection.GetTotalUserCount(ctx.DB)
	if err != nil {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}
	if total > 14 {
		ctx.WriteJson(w, r, errorAlready, nil)
		return
	}

	usernames := []string{
		"opal", "amber", "pyrite", "onyx", "morion", "quartz", "lavender", "tourmaline", "citrine", "chrysocolla", "carnelian", "jasper", "jade", "agate", "aventurine",
	}
	users := [15]*model.User{}
	//users := userst[0:]

	h, err := bcrypt.GenerateFromPassword([]byte("TestPassword"), bcrypt.DefaultCost)
	if err != nil {
		return
	}

	for i, name := range usernames {

		user := &model.User{
			Email:    name + "@giftsphere.com",
			Password: string(h),
			Name:     name,
		}

		err = usercollection.CreateUser(user, ctx.DB)
		if ctx.CheckError(w, r, err) {
			return
		}

		user, err = usercollection.GetUserByName(name, ctx.DB)
		if ctx.CheckError(w, r, err) {
			return
		}
		users[i] = user
	}

	for i := 0; i < 4; i++ {
		sphere := model.GiftSphere{}
		sphere.Type = 0
		sphere.Level = int64(i)
		sphere.Place[14] = users[0+i].ID
		sphere.Place[13] = users[1+i].ID
		sphere.Place[12] = users[2+i].ID
		sphere.Place[11] = users[3+i].ID
		sphere.Place[10] = users[4+i].ID
		sphere.Place[9] = users[5+i].ID
		sphere.Place[8] = users[6+i].ID

		err = sphere.PutInDatabase(ctx.DB)
		if ctx.CheckError(w, r, err) {
			return
		}
		err = sphere.CreateUserSpheres(ctx.DB)
		if ctx.CheckError(w, r, err) {
			return
		}
	}

	ctx.WriteJson(w, r, nil, "OK")
}
