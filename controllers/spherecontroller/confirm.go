package spherecontroller // Get godoc
import (
	"fmt"
	"giftsphere/controllers/controllermodel"
	"giftsphere/db"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"github.com/gocraft/web"
)

type СonfirmApiC struct{ controllermodel.Controller }

var СonfirmApiController = СonfirmApiC{}
var errorSomething = fmt.Errorf("Error Something Wrong!!! ")

// Get godoc
// @Summary Confirm member payment
// @Tags User
// @Produce json
// @Success 200 {object} model.GiftSphere
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/confirm [get]
func (c *СonfirmApiC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	sphereId, err := ctx.Parameters.GetInt64("sphere")
	if ctx.CheckError(w, r, err) {
		return
	}
	place, err := ctx.Parameters.GetInt64("place")
	if ctx.CheckError(w, r, err) {
		return
	}
	if place < 0 || place > 7 {
		ctx.WriteJson(w, r, errorSomething, nil)
		return
	}

	sphere, err := model.GetSphere(sphereId, ctx.DB)
	if err != nil {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}

	if sphere.Place[place] == 0 {
		ctx.WriteJson(w, r, errorSomething, nil)
		return
	}
	requester := sphere.GetUserPlace(ctx.Session.UserID)
	if requester != 14 {
		ctx.WriteJson(w, r, errorOnlySpheremaster, nil)
		return
	}
	if sphere.Payed[place] == true {
		ctx.WriteJson(w, r, errorSomething, nil)
		return
	}
	sphere.Payed[place] = true
	complete := true
	for i := 0; i < 8 && complete; i++ {
		complete = sphere.Payed[i]
	}

	if complete {
		err = SplitSphere(sphere, ctx.DB)
		if ctx.CheckError(w, r, err) {
			return
		}
	} else {
		err = sphere.PutInDatabase(ctx.DB)
		if ctx.CheckError(w, r, err) {
			return
		}
	}
	ctx.WriteJson(w, r, nil, sphere)
}

func SplitSphere(mother model.GiftSphere, db db.ConnDB) error {
	err := model.RemoveAllUserSpheres(mother.Id, db)
	if err != nil {
		return err
	}
	child1 := model.GiftSphere{
		Type:  mother.Type,
		Level: mother.Level,
		Place: [15]int64{
			0, 0, 0, 0, 0, 0, 0, 0,
			mother.Place[5],
			mother.Place[6],
			mother.Place[0],
			mother.Place[7],
			mother.Place[10],
			mother.Place[11],
			mother.Place[13],
		},
		Payed: [8]bool{},
	}
	err = child1.PutInDatabase(db)
	if err != nil {
		return err
	}
	err = child1.CreateUserSpheres(db)
	if err != nil {
		return err
	}

	child2 := model.GiftSphere{
		Type:  mother.Type,
		Level: mother.Level,
		Place: [15]int64{
			0, 0, 0, 0, 0, 0, 0, 0,
			mother.Place[3],
			mother.Place[4],
			mother.Place[1],
			mother.Place[2],
			mother.Place[8],
			mother.Place[9],
			mother.Place[12],
		},
		Payed: [8]bool{},
	}
	err = child2.PutInDatabase(db)
	if err != nil {
		return err
	}
	err = child2.CreateUserSpheres(db)
	if err != nil {
		return err
	}

	err = mother.RemoveSphere(db)
	if err != nil {
		return err
	}
	return err
}
