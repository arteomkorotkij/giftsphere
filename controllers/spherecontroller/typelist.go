package spherecontroller

import (
	"fmt"
	"giftsphere/db"
	"giftsphere/model"
)

type SphereTypes map[int]map[int]model.SphereType

var sphereTypes SphereTypes

func LoadSpheresTypes() (err error) {
	sT, err := model.GetSphereTypes(db.DB)
	if err != nil {
		return err
	}

	sphereTypes = SphereTypes{}
	for _, st := range sT {
		if _, OK := sphereTypes[st.Type]; !OK {
			sphereTypes[st.Type] = map[int]model.SphereType{}
		}
		sphereTypes[st.Type][st.Level] = st
	}
	return
}
func GetSpheresTypes() SphereTypes {
	if sphereTypes == nil || len(sphereTypes) == 0 {
		err := LoadSpheresTypes()
		if err != nil {
			fmt.Println("Error LoadSpheresTypes: ", err)
			sphereTypes = SphereTypes{}
		}
	}
	return sphereTypes
}

/*
// Get godoc
// @Summary Getting all spheres types
// @Tags User
// @Produce json
// @Success 200 {object} SphereTypes
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/spheretypes [get]
func (c *SphereTypesC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	if sphereTypes == nil {
		LoadSpheresTypes()
	}
	ctx.WriteJson(w, r, nil, sphereTypes)
}
*/
