package spherecontroller

import (
	"fmt"
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"github.com/gocraft/web"
)

type DriveOutApiC struct{ controllermodel.Controller }

var DriveOutApiController = DriveOutApiC{}
var errorOnlySpheremaster = fmt.Errorf("Only Spheremaster can do that!!! ")
var errorOnlyOuterRing = fmt.Errorf("Only uoter ring members can be drive out!!! ")

// Get godoc
// @Summary Drive out member of sphere
// @Tags User
// @Produce json
// @Success 200 {object} model.GiftSphere
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/driveout [get]
func (c *DriveOutApiC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	sphereId, err := ctx.Parameters.GetInt64("sphere")
	if ctx.CheckError(w, r, err) {
		return
	}
	place, err := ctx.Parameters.GetInt64("place")
	if ctx.CheckError(w, r, err) {
		return
	}
	if place < 0 || place > 7 {
		ctx.WriteJson(w, r, errorOnlyOuterRing, nil)
		return
	}

	sphere, err := model.GetSphere(sphereId, ctx.DB)
	if err != nil {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}

	if sphere.Place[place] == 0 {
		ctx.WriteJson(w, r, errorOnlyOuterRing, nil)
		return
	}
	requester := sphere.GetUserPlace(ctx.Session.UserID)
	if requester != 14 {
		ctx.WriteJson(w, r, errorOnlySpheremaster, nil)
		return
	}
	us := model.UserSphere{
		SphereId: sphere.Id,
		UserId:   sphere.Place[place],
		Level:    int(sphere.Level),
		Type:     int(sphere.Type),
	}
	err = model.RemoveUserSphere(us, ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}

	sphere.Place[place] = 0
	err = sphere.PutInDatabase(ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}
	ctx.WriteJson(w, r, nil, sphere)
}
