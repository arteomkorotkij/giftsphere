package spherecontroller

import (
	"fmt"
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"github.com/gocraft/web"
	"strconv"
)

type InvitationApiC struct{ controllermodel.Controller }

var InvitationApiController = InvitationApiC{}
var errorIncorrect = fmt.Errorf("Incorrect Invitation!!! ")

// Get godoc
// @Summary Apply Invitation
// @Tags User
// @Produce json
// @Success 200 {object} model.GiftSphere
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/invitation [get]
func (c *InvitationApiC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	invitation, err := ctx.Parameters.GetString("inv")
	sphereId, err := strconv.ParseInt(invitation[0:len(invitation)-2], 10, 64)
	if err != nil {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}
	place, err := strconv.ParseInt(invitation[len(invitation)-2:], 10, 64)
	if err != nil {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}
	if place < 0 || place > 14 {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}
	sphere, err := model.GetSphere(sphereId, ctx.DB)
	if err != nil {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}
	if sphere.Place[place] != 0 {
		ctx.WriteJson(w, r, errorIncorrect, nil)
		return
	}

	haveSame, err := model.HaveUserSameSphere(ctx.Session.UserID, int(sphere.Type), int(sphere.Level), ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}
	if haveSame {
		ctx.WriteJson(w, r, fmt.Errorf("You are already in the same sphere!!! "), nil)
		return
	}
	us := model.UserSphere{
		SphereId: sphere.Id,
		UserId:   ctx.Session.UserID,
		Level:    int(sphere.Level),
		Type:     int(sphere.Type),
	}
	err = model.AddUserSphere(us, ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}
	sphere.Place[place] = ctx.Session.UserID
	err = sphere.PutInDatabase(ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}
	ctx.WriteJson(w, r, nil, sphere)
}
