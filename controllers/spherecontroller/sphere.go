package spherecontroller

import (
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"github.com/gocraft/web"
)

type SphereApiC struct{ controllermodel.Controller }

var SphereApiController = SphereApiC{}

// Get godoc
// @Summary Create new sphere (Only Admin)
// @Tags User
// @Produce json
// @Success 200 {object} "Created"
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/sphere [post]
func (c *SphereApiC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	/*if !ctx.Session.IsAdmin {
		ctx.WriteJson(w, r, fmt.Errorf("You are not admin!!! "), nil)
		return
	}*/
	sphere := model.GiftSphere{}
	sphere.Place[14] = ctx.Session.UserID
	err := sphere.PutInDatabase(ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}
	err = sphere.CreateUserSpheres(ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}
	ctx.WriteJson(w, r, nil, "Created")
}
