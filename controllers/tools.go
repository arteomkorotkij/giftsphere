package controllers

import (
	"net/http"
	"strconv"

	"github.com/gocraft/web"

	"giftsphere/ectx"
	"giftsphere/router"
)

func ConfirmRedirectMsg(
	ctx *ectx.Context,
	w *router.ResponseGZipWriter,
	r *web.Request,
	msgKey string,
	confirmKey string,
	ID int64,
) {
	var confirm string
	if ID != 0 && confirmKey != "" {
		confirm = "&" + confirmKey + "=" + strconv.Itoa(int(ID))
	}

	http.Redirect(
		w,
		r.Request,
		"/"+ctx.Lang+"/msg?msg-key="+msgKey+confirm,
		http.StatusFound,
	)
}
