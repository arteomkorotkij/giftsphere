package aapi

import (
	"time"

	"github.com/gocraft/web"

	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/router"
)

type ReloadC struct{ controllermodel.Controller }

var ReloadController = ReloadC{}

var ReloadChan = make(chan bool)

// Get godoc
// @Summary Getting reload controller
// @Tags Admin
// @Produce json
// @Success 200 {object} controllermodel.Msg
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /admin/reload [get]
func (c *ReloadC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	ctx.WriteJson(w, r, nil, controllermodel.Msg{Status: true, Msg: "reload start"})
	go func() {
		time.Sleep(time.Second)
		ReloadChan <- true
	}()
}
