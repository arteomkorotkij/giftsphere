package capi

import (
	"golang.org/x/crypto/bcrypt"

	"github.com/gocraft/web"

	"giftsphere/collections/usercollection"
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"giftsphere/session"
)

type ReqCreateUser struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

type SignUpC struct{ controllermodel.Controller }

var SignUpController = SignUpC{}

// Post godoc
// @Summary Posting signup controller
// @Tags Auth
// @Produce json
// @Param ReqCreateUser body ReqCreateUser true "Req Create User"
// @Success 200 {object} LoginResp
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/signup [post]
func (c *SignUpC) Post(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	req := ReqCreateUser{}
	err := ctx.ParseJson(r, &req)
	if ctx.CheckError(w, r, err) {
		return
	}

	tx, err := ctx.GetTx()
	if ctx.CheckError(w, r, err) {
		return
	}
	defer func() {
		_ = tx.Tx.Rollback()
	}()

	h, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return
	}

	user := &model.User{
		Email:    req.Email,
		Password: string(h),
		Name:     req.Name,
	}

	err = usercollection.CreateUser(user, ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}

	if ctx.Session.SessID != "" {
		err = session.Del(ctx.Context, ctx.Session.SessID)
		if ctx.CheckError(w, r, err) {
			return
		}
	}

	ctx.Session.GenSessionID(user.ID)
	ctx.Session.UserID = user.ID
	err = ctx.Session.Update(w)
	if ctx.CheckError(w, r, err) {
		return
	}

	_ = tx.Tx.Commit()
	ctx.WriteJson(w, r, nil, LoginResp{
		SessionID: ctx.Session.SessID,
		User:      user,
	})
}
