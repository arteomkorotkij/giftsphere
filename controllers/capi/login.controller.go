package capi

import (
	"errors"
	"golang.org/x/crypto/bcrypt"

	"github.com/gocraft/web"

	"giftsphere/collections/usercollection"
	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/model"
	"giftsphere/router"
	"giftsphere/session"
)

type LoginC struct{ controllermodel.Controller }

type LoginResp struct {
	SessionID string      `json:"sessionId"`
	User      *model.User `json:"user"`
}

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

var LoginController = LoginC{}

// Post godoc
// @Summary Posting login controller
// @Tags Auth
// @Produce json
// @Param Login body Login true "Login body"
// @Success 200 {object} LoginResp
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/login [post]
func (c *LoginC) Post(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	login := Login{}
	err := ctx.ParseJson(r, &login)
	if ctx.CheckError(w, r, err) {
		return
	}

	user, err := usercollection.GetUserByEmail(login.Email, ctx.DB)
	if ctx.CheckError(w, r, err) {
		return
	}

	if !checkPassword(user.Password, login.Password) {
		if ctx.CheckError(w, r, errors.New("bad email or password")) {
			return
		}
	}

	if ctx.Session.SessID != "" {
		err = session.Del(ctx.Context, ctx.Session.SessID)
		if ctx.CheckError(w, r, err) {
			return
		}
	}

	ctx.Session.GenSessionID(user.ID)
	ctx.Session.UserID = user.ID
	err = ctx.Session.Update(w)
	if ctx.CheckError(w, r, err) {
		return
	}

	ctx.WriteJson(w, r, nil, LoginResp{
		SessionID: ctx.Session.SessID,
		User:      user,
	})
}

func checkPassword(hashedPassword, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err == nil
}
