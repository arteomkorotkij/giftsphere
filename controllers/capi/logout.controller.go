package capi

import (
	"github.com/gocraft/web"

	"giftsphere/controllers/controllermodel"
	"giftsphere/ectx"
	"giftsphere/router"
	"giftsphere/services/sessionservice"
)

type LogoutC struct{ controllermodel.Controller }

var LogoutController = LogoutC{}

// Get godoc
// @Summary logout controller
// @Tags Auth
// @Produce json
// @Success 200 {object} controllermodel.Msg
// @Failure 400 {object} errorR.Error
// @Security SessionID
// @Security ApiKeyAuth
// @Router /api/logout [get]
func (c *LogoutC) Get(ctx *ectx.Context, w *router.ResponseGZipWriter, r *web.Request) {
	err := sessionservice.DeleteSession(w, ctx.Context, ctx.Session)
	if ctx.CheckError(w, r, err) {
		return
	}

	ctx.WriteJson(w, r, nil, controllermodel.Msg{
		Status: true,
		Msg:    "success",
	})
}
