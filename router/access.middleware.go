package router

import (
	"net/http"
	"strings"

	"github.com/gocraft/web"

	"giftsphere/ectx"
	"giftsphere/errorR"
	"giftsphere/role"
)

func getAccess(onlyRoles []role.Role) func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {

	return func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
		for _, r := range onlyRoles {
			if r == role.Admin {
				if !c.Session.IsAdmin && !strings.HasPrefix(req.URL.Path, "/admin-ui/") && req.URL.Path != "/admin/login" {
					c.WriteJson(rw, req, &errorR.Error{Msg: "don't have access", Status: http.StatusUnauthorized}, nil)
					return
				}
			}
		}
		//for _, r := range onlyRoles {
		//	if c.Session.User.HasRole(r) {
		//		next(rw, req)
		//		return
		//	}
		//}
		//
		//c.WriteJson(rw, req, &errorR.Error{Msg: "don't have access", Status: http.StatusForbidden}, nil)
		next(rw, req)
	}

}
