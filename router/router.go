package router

import (
	"context"
	"crypto/tls"
	"fmt"
	"giftsphere/model"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/gocraft/web"
	"golang.org/x/crypto/acme/autocert"

	"giftsphere/conf"
	"giftsphere/ectx"
	"giftsphere/role"
)

// Errors
var (
	ErrConfigHostEmpty = fmt.Errorf("config: 'ui-admin-host' or 'ui-customer-host' is empty")
)

// Routers
var (
	redirectRouter *web.Router //nolint
	publicRouter   *web.Router
	privateRouter  *web.Router
	redirects      []string
)

func init() {
	publicRouter = web.New(ectx.Context{})
	privateRouter = web.New(ectx.Context{})
	redirectRouter = web.New(ectx.Context{})

	if conf.IsDev() {
		publicRouter = publicRouter.Middleware(web.ShowErrorsMiddleware)
	}
}

func InitRedirect() {
	redirects = strings.Split(conf.Config.Str("redirects"), ",")
	for i, l := 0, len(redirects); i < l; i++ {
		redirects = append(redirects, "www."+redirects[i])
	}
}

func GetRouter() *web.Router {
	return publicRouter
}

func InitPrivateController(path string, handler ControllerI) {
	subroute := privateRouter.Subrouter(ectx.Context{}, path)

	subroute.
		Middleware(initContext(120, "")).
		Middleware(getController(handler)).
		Get("", EmptyFunc).Post("", EmptyFunc).Put("", EmptyFunc).Delete("", EmptyFunc)
}

func InitPublicControllerWithLang(path, domain string, handler ControllerI, onlyRoles ...role.Role) {
	InitPublicController(path, domain, "", handler, onlyRoles...)
	// InitPublicController("/:lang"+path, domain, handler, onlyRoles...)
	for _, lang := range model.LangList {
		InitPublicController("/"+string(lang)+path, domain, string(lang), handler, onlyRoles...)

	}
}

func InitPublicController(path, domain, lang string, handler ControllerI, onlyRoles ...role.Role) {
	subroute := publicRouter.Subrouter(ectx.Context{}, path)
	timeout := time.Duration(120)

	for _, r := range onlyRoles {
		if r == role.Admin {
			timeout = time.Duration(600)
			break
		}
	}

	if len(redirects) > 0 {
		subroute = subroute.Middleware(redirect(redirects, domain))
	}

	if domain != "" && conf.Config.Str("tls_use") == "on" {
		subroute = subroute.Middleware(getDomenAccess(domain))
	}

	//if !conf.IsDev() {
	//	//subroute = subroute.Middleware(basicAuth)
	//}
	subroute = subroute.Middleware(debug)

	if conf.Config.Str("redis_use") == "on" && path != "/static/:*" && path != "/static" {
		subroute = subroute.Middleware(getSession)
	} else {
		subroute = subroute.Middleware(noSession)
	}

	if !conf.Production {
		subroute.Middleware(initSwagger)
	}

	if len(onlyRoles) > 0 {
		subroute = subroute.Middleware(getAccess(onlyRoles))
	}

	subroute.Middleware(initContext(timeout, lang))

	subroute.
		Middleware(getController(handler)).
		Get("", EmptyFunc).Post("", EmptyFunc).Put("", EmptyFunc).Delete("", EmptyFunc)
}

func EmptyFunc(c *ectx.Context, rw web.ResponseWriter, req *web.Request) {
}

var httpSwaggerHandler = httpSwagger.Handler()

func SwaggerFunc(c *ectx.Context, rw web.ResponseWriter, req *web.Request) {
	httpSwaggerHandler(rw, req.Request)
}

func Run(publicPort, customerHost string) <-chan bool {
	if !conf.Production {
		subroute := publicRouter.Subrouter(ectx.Context{}, "/swagger")
		subroute.Get("", SwaggerFunc)
		subroute = publicRouter.Subrouter(ectx.Context{}, "/swagger/:*")
		subroute.Get("", SwaggerFunc)
	}

	interrupt := make(chan os.Signal, 2)
	signal.Notify(interrupt,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	var srvPublic *http.Server
	var srvPublicTLS *http.Server
	done := make(chan bool, 1)

	if conf.Config.Str("tls_use") == "on" { // Use TLS
		if customerHost == "" {
			panic(ErrConfigHostEmpty)
		}

		whiteList := append(redirects, customerHost)
		m := &autocert.Manager{
			Cache:      TLSCache{},
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(whiteList...),
		}
		srvPublicTLS = &http.Server{
			Addr:         ":443",
			ErrorLog:     NewHTTPLoger(conf.Config.Str("http_errlog_path"), "https: "),
			Handler:      publicRouter,
			IdleTimeout:  time.Minute * 5,
			ReadTimeout:  time.Minute * 5,
			WriteTimeout: time.Minute * 5,
			//MaxHeaderBytes: 1024 * 1024 * 100,
			TLSConfig: &tls.Config{
				GetCertificate: m.GetCertificate,
			},
		}
		srvPublic = &http.Server{
			Addr:        ":80",
			ErrorLog:    NewHTTPLoger(conf.Config.Str("http_errlog_path"), "http: "),
			IdleTimeout: time.Minute * 5,
			Handler:     m.HTTPHandler(nil),
		}

		go func() {
			fmt.Println(srvPublicTLS.ListenAndServeTLS("", ""))
		}()
		go func() {
			fmt.Println(srvPublic.ListenAndServe())
		}()
		fmt.Println("The https service is ready to listen and serve.")
	} else { // Don't use TLS
		srvPublic = &http.Server{
			Addr:        publicPort,
			IdleTimeout: time.Minute * 5,
			Handler:     publicRouter,
		}

		go func() {
			fmt.Println(srvPublic.ListenAndServe())
		}()
		fmt.Println("The http service is ready to listen and serve. PORT: " + publicPort)
	}

	// Listener close
	go func() {
		sig := <-interrupt

		fmt.Println(sig)
		_ = srvPublic.Shutdown(context.Background())
		if srvPublicTLS != nil {
			_ = srvPublicTLS.Shutdown(context.Background())
		}

		done <- true
	}()

	fmt.Println("awaiting signal")
	return done
}
