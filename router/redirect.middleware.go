package router

import (
	"net/http"

	"github.com/gocraft/web"

	"giftsphere/ectx"
	"giftsphere/tools"
)

func redirect(redirects []string, redirectTo string) func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	return func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
		if tools.IncludeStr(redirects, req.Host) {
			req.URL.Host = redirectTo
			req.URL.Scheme = "https"
			http.Redirect(rw, req.Request, req.URL.String(), http.StatusMovedPermanently)
		} else {
			next(rw, req)
		}
	}
}
