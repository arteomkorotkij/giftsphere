package router

import (
	"net/http"

	"github.com/gocraft/web"

	"giftsphere/ectx"
)

func getDomenAccess(onlyDomain string) func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {

	return func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
		if req.Host == onlyDomain {
			next(rw, req)
			return
		}

		rw.WriteHeader(http.StatusBadGateway)
	}

}
