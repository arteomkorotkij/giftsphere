package router

import (
	"context"
	"fmt"
	"giftsphere/model"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gocraft/web"

	"giftsphere/conf"
	"giftsphere/db"
	"giftsphere/ectx"
)

func GetParameters(req *web.Request) *ectx.Parameters {
	params := ectx.Parameters{}
	for k, v := range req.PathParams {
		if k == "*" {
			params["path"] = v
		} else {
			params[k] = v
		}
	}

	for k, v := range req.URL.Query() {
		if len(v) == 0 {
			params[k] = ""
		} else {
			params[k] = v[0]
		}
	}

	err := req.ParseForm()
	if err != nil {
		fmt.Println(err)
	}

	for k, v := range req.PostForm {
		if len(v) == 0 {
			params[k] = ""
		} else {
			params[k] = v[0]
		}
	}

	return &params
}

func initContext(timeoutSec time.Duration, lang string) func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	return func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
		var err error
		c.Parameters = GetParameters(req)
		if lang != "" {
			c.Parameters.Set("lang", lang)
		}

		c.Context, c.Stop = context.WithTimeout(req.Context(), time.Second*timeoutSec)
		interrupt := make(chan os.Signal, 2)
		signal.Notify(interrupt,
			syscall.SIGINT,
			syscall.SIGTERM,
			syscall.SIGQUIT)
		go func() {
			select {
			case <-interrupt:
				c.Stop()
			case <-c.Context.Done():
			}
		}()

		c.DB = &db.CtxDB{Context: c.Context, Stop: c.Stop}
		c.ProcessTimeLog = ectx.NewProcessTimeLog()

		lang, _ := c.Parameters.GetString("lang")
		if lang != "" && !model.SupportLang(model.Lang(lang)) {
			lang = string(model.LangEn)
		}

		c.Lang = lang
		if c.Lang == "" && c.Session.User.Lang != "" {
			c.Lang = c.Session.User.Lang
		}
		c.PathLang, _ = c.Parameters.GetString("lang")

		defer c.Stop()
		next(rw, req)
		if c.HaveTx() {
			tx, _ := c.GetTx()
			_ = tx.Tx.Commit()
		}

		if !c.IsContextError && conf.UseProcessTimeLog() {
			if err = c.ProcessTimeLog.WriteToDB(); err != nil {
				fmt.Println(err)
			}
		}
	}
}
