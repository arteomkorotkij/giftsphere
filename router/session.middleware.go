package router

import (
	"context"
	"fmt"

	"github.com/gocraft/web"

	"giftsphere/collections/usercollection"
	"giftsphere/db"
	"giftsphere/ectx"
	"giftsphere/services/sessionservice"
	"giftsphere/session"
)

func getSession(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	s, err := sessionservice.GetSession(req, req.Context())
	if err != nil {
		if err == context.Canceled || err == context.DeadlineExceeded {
			return
		}
		fmt.Println(err)
	}

	if s == nil {
		s, err = sessionservice.TemporarySession(rw, req.Context())
		if err != nil {
			if err != context.Canceled && err != context.DeadlineExceeded {
				fmt.Println(err)
			}
			return
		}
	}

	if s.UserID != 0 {
		user, err := usercollection.GetUserByID(s.UserID, db.PlainDB)
		if err == nil {
			s.User = user
		} else {
			s, err = sessionservice.TemporarySession(rw, req.Context())
			if err != nil {
				if err != context.Canceled && err != context.DeadlineExceeded {
					fmt.Println(err)
				}
				return
			}
		}
	}

	c.Session = s
	next(rw, req)
}

func noSession(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	c.Session = session.NewSession(req.Context())
	c.Session.DontUpdate()
	c.Session.SessID = "dev-mode"
	//c.Session.User.Roles = []role.Role{role.Admin, role.Translator, role.Merchandiser, role.Designer, role.Developer, role.Manager, role.Sysadmin, role.User}
	next(rw, req)
}
