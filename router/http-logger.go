package router

import (
	"fmt"
	"log"
	"os"
)

type HTTPLogger struct {
	pathToFile string
}

func NewHTTPLoger(pathToFile, prefix string) *log.Logger {
	return log.New(&HTTPLogger{pathToFile: pathToFile}, prefix, 0)
}

func (l *HTTPLogger) Write(b []byte) (lb int, err error) {
	if l.pathToFile == "-" || l.pathToFile == "" {
		fmt.Println(string(b))
	} else {
		f, err := os.OpenFile(l.pathToFile, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			return 0, err
		}
		defer f.Close()

		_, err = f.WriteString("\n" + string(b))
		if err != nil {
			return 0, err
		}
		return 0, nil
	}

	return
}
