package router

import (
	"compress/gzip"
	"fmt"
	"github.com/andybalholm/brotli"
	"strconv"
	"strings"

	"github.com/gocraft/web"

	"giftsphere/ectx"
)

type ControllerI interface {
	// next web.NextMiddlewareFunc - don't use it.
	Get(c *ectx.Context, rw *ResponseGZipWriter, req *web.Request)
	Post(c *ectx.Context, rw *ResponseGZipWriter, req *web.Request)
	Put(c *ectx.Context, rw *ResponseGZipWriter, req *web.Request)
	Delete(c *ectx.Context, rw *ResponseGZipWriter, req *web.Request)
}

var (
	DefaultMinSize  = 1400
	DefaultQValue   = 1.0
	vary            = "Vary"
	acceptEncoding  = "Accept-Encoding"
	contentEncoding = "Content-Encoding"
	contentLength   = "Content-Length"
)

type ResponseGZipWriter struct {
	web.ResponseWriter
	rw                     web.ResponseWriter
	req                    *web.Request
	buffer                 []byte
	compressorGzip         *gzip.Writer
	compressorBrotli       *brotli.Writer
	notUseCompression      bool
	isWriteContentEncoding bool
	statusCode             int
	AcceptsGzip            bool
	AcceptsBrotli          bool
	LevelGzip              int
	LevelBrotli            int
}

func (writer *ResponseGZipWriter) NoCompression() {
	writer.notUseCompression = true
}

func (writer *ResponseGZipWriter) WriteLevel(levelGzip, levelBrotli int) {
	writer.LevelGzip = levelGzip
	writer.LevelBrotli = levelBrotli
	writer.notUseCompression = levelGzip == gzip.NoCompression
}

func (writer *ResponseGZipWriter) WriteHeader(statusCode int) {
	writer.statusCode = statusCode
}

func (writer *ResponseGZipWriter) Write(p []byte) (int, error) {
	if writer.notUseCompression {
		if writer.statusCode != 0 {
			writer.rw.WriteHeader(writer.statusCode)
			writer.statusCode = 0
		}

		return writer.rw.Write(p)
	}

	if writer.isWriteContentEncoding {
		if writer.compressorBrotli != nil {
			return writer.compressorBrotli.Write(p)
		} else if writer.compressorGzip != nil {
			return writer.compressorGzip.Write(p)
		}
	}

	writer.buffer = append(writer.buffer, p...)
	if len(writer.buffer) < DefaultMinSize {
		return len(p), nil
	}

	writer.rw.Header().Add(vary, acceptEncoding)
	if writer.AcceptsBrotli {
		writer.rw.Header().Add(contentEncoding, "br")
	} else if writer.AcceptsGzip {
		writer.rw.Header().Add(contentEncoding, "gzip")
	}
	writer.rw.Header().Del(contentLength)
	writer.rw.WriteHeader(writer.statusCode)
	writer.statusCode = 0

	if writer.AcceptsBrotli {
		writer.compressorBrotli = brotli.NewWriterOptions(writer.rw, brotli.WriterOptions{
			Quality: writer.LevelBrotli,
		})
	} else if writer.AcceptsGzip {
		compressorGzip, err := gzip.NewWriterLevel(writer.rw, writer.LevelGzip)
		if err != nil {
			return 0, err
		}
		writer.compressorGzip = compressorGzip
	}

	writer.isWriteContentEncoding = true
	buf := writer.buffer
	writer.buffer = nil

	if writer.compressorBrotli != nil {
		return writer.compressorBrotli.Write(buf)
	}
	return writer.compressorGzip.Write(buf)
}

func (writer *ResponseGZipWriter) Close() (l int, err error) {
	if len(writer.buffer) > 0 {
		if writer.statusCode != 0 {
			writer.rw.WriteHeader(writer.statusCode)
			writer.statusCode = 0
		}

		l, err = writer.rw.Write(writer.buffer)
		if err != nil {
			return
		}
	}

	if writer.compressorGzip != nil {
		return l, writer.compressorGzip.Close()
	} else if writer.compressorBrotli != nil {
		return l, writer.compressorBrotli.Close()
	}
	return l, nil
}

func NewGZipWriter(rw web.ResponseWriter, req *web.Request, levelGzip, levelBrotli int) (gzipWriter *ResponseGZipWriter, err error) {
	gzipWriter = &ResponseGZipWriter{
		ResponseWriter:    web.ResponseWriter(rw),
		rw:                rw,
		req:               req,
		buffer:            []byte{},
		AcceptsGzip:       AcceptsGzip(req),
		AcceptsBrotli:     AcceptsBrotli(req),
		notUseCompression: levelGzip == gzip.NoCompression,
		LevelGzip:         levelGzip,
		LevelBrotli:       levelBrotli,
		statusCode:        200,
	}

	if !gzipWriter.AcceptsBrotli && !gzipWriter.AcceptsGzip {
		gzipWriter.NoCompression()
	}

	return
}

type codings map[string]float64

// AcceptsGzip returns true if the given HTTP request indicates that it will
// accept a gzipped response.
func AcceptsGzip(r *web.Request) bool {
	acceptedEncodings, _ := parseEncodings(r.Header.Get(acceptEncoding))
	return acceptedEncodings["gzip"] > 0.0
}

// AcceptsBrotli returns true if the given HTTP request indicates that it will
// accept a brotled response.
func AcceptsBrotli(r *web.Request) bool {
	acceptedEncodings, _ := parseEncodings(r.Header.Get("Accept-Encoding"))
	return acceptedEncodings["br"] > 0.0
}

// parseEncodings attempts to parse a list of codings, per RFC 2616, as might
// appear in an Accept-Encoding header. It returns a map of content-codings to
// quality values, and an error containing the errors encountered. It's probably
// safe to ignore those, because silently ignoring errors is how the internet
// works.
//
// See: http://tools.ietf.org/html/rfc2616#section-14.3.
func parseEncodings(s string) (codings, error) {
	c := make(codings)
	var e []string

	for _, ss := range strings.Split(s, ",") {
		coding, qvalue, err := parseCoding(ss)

		if err != nil {
			e = append(e, err.Error())
		} else {
			c[coding] = qvalue
		}
	}

	// Use a proper multi-error struct, so the individual errors
	// can be extracted if anyone cares.
	if len(e) > 0 {
		return c, fmt.Errorf("errors while parsing encodings: %s", strings.Join(e, ", "))
	}

	return c, nil
}

// parseCoding parses a single conding (content-coding with an optional qvalue),
// as might appear in an Accept-Encoding header. It attempts to forgive minor
// formatting errors.
func parseCoding(s string) (coding string, qvalue float64, err error) {
	for n, part := range strings.Split(s, ";") {
		part = strings.TrimSpace(part)
		qvalue = DefaultQValue

		if n == 0 {
			coding = strings.ToLower(part)
		} else if strings.HasPrefix(part, "q=") {
			qvalue, err = strconv.ParseFloat(strings.TrimPrefix(part, "q="), 64)

			if qvalue < 0.0 {
				qvalue = 0.0
			} else if qvalue > 1.0 {
				qvalue = 1.0
			}
		}
	}

	if coding == "" {
		err = fmt.Errorf("empty content-coding")
	}

	return
}
