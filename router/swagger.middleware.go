package router

import (
	"strconv"

	"github.com/gocraft/web"

	"giftsphere/collections/usercollection"
	"giftsphere/db"
	"giftsphere/ectx"
	"giftsphere/errorR"
	"giftsphere/model"
	"giftsphere/role"
)

func initSwagger(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	userIDStr := req.Header.Get("SwaggerAuthUserID")
	if userIDStr != "" {
		userID, err := strconv.Atoi(userIDStr)
		if err != nil {
			c.WriteJson(rw, req, &errorR.Error{Msg: "Error SwaggerAuthUserID header: " + err.Error()}, nil)
			return
		}

		if userID == -1 {
			c.Session.User = &model.User{
				Role: role.Admin,
				Lang: string(model.LangEn),
			}
			c.Session.Role = role.Admin
			c.Session.UserID = -1
			c.Session.IsAdmin = true
		} else {
			user, err := usercollection.GetUserByID(int64(userID), db.PlainDB)
			if err != nil {
				c.WriteJson(rw, req, &errorR.Error{Msg: "Error SwaggerAuthUserID header: " + err.Error()}, nil)
				return
			}

			c.Session.User = user
			c.Session.Role = user.Role
			c.Session.UserID = user.ID
			c.Session.IsAdmin = true
		}
	}

	next(rw, req)
}
