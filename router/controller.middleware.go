package router

import (
	"compress/gzip"
	"github.com/andybalholm/brotli"
	"net/http"

	"github.com/gocraft/web"

	"giftsphere/ectx"
)

func getController(handler ControllerI) func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	return func(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
		method := req.Method
		rwGzip, err := NewGZipWriter(rw, req, gzip.DefaultCompression, brotli.DefaultCompression)
		if err != nil {
			panic(err)
		}
		defer rwGzip.Close()

		if method == "POST" {
			handler.Post(c, rwGzip, req)
		} else if method == "GET" {
			handler.Get(c, rwGzip, req)
		} else if method == "PUT" {
			handler.Put(c, rwGzip, req)
		} else if method == "DELETE" {
			handler.Delete(c, rwGzip, req)
		} else {
			rw.WriteHeader(http.StatusMethodNotAllowed)
		}
	}
}
