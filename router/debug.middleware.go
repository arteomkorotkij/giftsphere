package router

import (
	"time"

	"github.com/gocraft/web"

	"giftsphere/ectx"
	"giftsphere/services/statisticsservice"
	"giftsphere/tools"
)

func debug(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	u := req.Request.URL.Path

	if tools.ContainsOnly(u, "/debug/vars", "/static/", "/admin", "/debug") {
		next(rw, req)
	} else {
		statisticsservice.IncCurrentConnection()
		t1 := time.Now()
		next(rw, req)
		statisticsservice.DecCurrentConnection(time.Since(t1))
	}
}
