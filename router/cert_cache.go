package router

import (
	"context"
	"encoding/base64"

	"giftsphere/db"
	"giftsphere/tools"
	"golang.org/x/crypto/acme/autocert"
)

// TLSCache implements Cache using a directory on the local filesystem.
// If the directory does not exist, it will be created with 0700 permissions.
type TLSCache struct{}

const TLSCachePrefix = "tls_key_prefix_"

// Get reads a certificate data from the specified file name.
func (d TLSCache) Get(ctx context.Context, name string) (b []byte, err error) {
	value, _ := tools.GetSetting(db.PlainDB, TLSCachePrefix+name)
	if value == "" {
		return nil, autocert.ErrCacheMiss
	}

	return base64.StdEncoding.DecodeString(value)
}

func (d TLSCache) Put(ctx context.Context, name string, data []byte) (err error) {
	value := base64.StdEncoding.EncodeToString(data)
	return tools.SetSetting(db.PlainDB, TLSCachePrefix+name, value)
}

// Delete removes the specified file name.
func (d TLSCache) Delete(ctx context.Context, name string) (err error) {
	return tools.DeleteSetting(db.PlainDB, TLSCachePrefix+name)
}
