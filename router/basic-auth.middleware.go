package router

import (
	"crypto/subtle"
	"github.com/gocraft/web"

	"giftsphere/ectx"
)

func basicAuth(c *ectx.Context, rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) { //nolint
	user, pass, ok := req.BasicAuth()

	if !ok || subtle.ConstantTimeCompare([]byte(user), []byte("getfitshape")) != 1 || subtle.ConstantTimeCompare([]byte(pass), []byte("getfitshape")) != 1 {
		rw.Header().Set("WWW-Authenticate", `Basic realm="Please enter your username and password for this site"`)
		rw.WriteHeader(401)
		_, _ = rw.Write([]byte(`{"msg":"Unauthorised."}`))
		return
	}

	next(rw, req)
}
