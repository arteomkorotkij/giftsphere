#!/bin/bash

yarn install
yarn build:linux

gzip -c ./sitemap-indexer | ssh ubuntu@104.131.191.83 "cat > /home/ubuntu/sitemap-indexer.gz; rm /home/ubuntu/sitemap-indexer; gunzip -c /home/ubuntu/sitemap-indexer.gz > /home/ubuntu/sitemap-indexer; chmod +x /home/ubuntu/sitemap-indexer;"
