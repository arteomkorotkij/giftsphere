require('dotenv').config();
const path = require('path');
const fs = require('fs');
const SitemapGenerator = require('sitemap-generator');
const AWS = require('aws-sdk');
//

const sitemapPath = 'sitemap.xml';
const timeSleepSec = parseInt(process.env.TIME_SLEEP_SEC, 10);
const once = process.env.ONCE === 'true';

run();
function run() {
    const generator = SitemapGenerator(process.env.SITE_URL, {
        maxEntriesPerFile: 1000000,
        filepath: path.join(process.cwd(), sitemapPath),
        stripQuerystring: true,
    });

    generator.on('done', () => {
        // sitemaps created
        consoleLog('done generate sitemap!');

        const sitemapData = fs.readFileSync(path.join(process.cwd(), sitemapPath), {encoding: 'utf-8'});

        const s3 = new AWS.S3({
            endpoint: process.env.CLOUD_ENDPOINT,
            accessKeyId: process.env.CLOUD_KEY_ID,
            secretAccessKey: process.env.CLOUD_KEY_SECRET,
            region: process.env.CLOUD_REGION,
        });

        const params = {
            Bucket: process.env.CLOUD_BUCKET,
            Key: sitemapPath,
            Body: sitemapData,
            ACL: 'public-read',
        };

        s3.upload(params, function (err, data) {
            if (err) {
                throw err;
            }
            consoleLog(`File uploaded successfully. ${data.Location}`);
        });

        if (once) {
            return;
        }

        setTimeout(run, timeSleepSec * 1000);
    });

    consoleLog('start!');
    generator.start();
}

function consoleLog(...args) {
    console.log(new Date().toString() + ': ', ...args);
}
